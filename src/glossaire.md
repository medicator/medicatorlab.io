# Glossaire
###### Patient
Personne qui prend les médicaments.

###### Soignant
Personne intéressée à savoir que le patient a pris ses médicaments. Ceci peut-être un pharmacien ou encore un proche du patient.

###### Pharmacien
Personne ou organisation qui doit préparer les médicaments du patient.

###### Attaquant
Personne malveillante tentant de déjouer le système afin de lui faire distribuer des médicaments à l'infini ou encore de l'empêcher de distribuer des médicaments.  

La plus grande menace de ce système est le patient lui-même qui pourrait vouloir distribuer des médicaments à l'infini afin de pouvoir les revendre sans avoir à briser son appareil.
