# Cahier des charges

## Contexte
Les médicaments doivent être pris à des heures précises de la journée. Les personnes âgées, surtout celles souffrant de troubles de la mémoire, peuvent facilement oublier de prendre leurs médicaments ou les prendre en retard. Dans cette situation, il est difficile pour un médecin ou la famille d'un patient de s'en apercevoir. À l'heure actuelle, la meilleure façon de s'assurer qu'un patient prend ses médicaments à l'heure est de faire venir une infirmière pour lui faire prendre.  

En plus de pouvoir être oubliés, les doses de médicaments peuvent être prises plusieurs fois par des personnes ayant des trouble de la mémoire ce qui peut leur être fatal. Le patient dépassant intentionnellement sa dose ainsi que pour les enfants, même s'ils ne consomment qu'une seule dose destinée à un adulte, sont aussi à risque. Il est à noter que les mécanismes actuellement utilisés pour garder les médicaments hors de la portée des enfants sont très peu sécuritaires et ceux destinés à empêcher la consommation de ses médicaments par d'autres adultes sont inexistants.  

Afin de les aider à se concentrer à l'école, beaucoup de jeunes ont des prescriptions d'amphétamines et vont sauter des jours de leur prescription afin de pouvoir prendre plusieurs fois leur dose journalière d'un seul coup pour avoir un «buzz».  

## Cibles
* Toutes personnes prenant des médicaments
* Les personnes âgées: qui doivent se faire rappeler de prendre leurs médicaments, et qui pourraient prendre leurs médicaments plus d'une fois
* Les personnes ayant des problèmes d'addiction: qui risques d'intentionnellement prendre plus que leur dose. (Les adolescent peuvent aussi avoir ce genre de comportement)

## Besoins
* Rappeler au patient de prendre ses médicaments à la bonne heure
* Empêcher le patient de prendre plusieurs fois sa prescription
* Empêcher l'accès aux médicaments aux personnes autres que le patient (ex.: enfants)
* Avertir la famille et, ou le soignant du patient lorsqu'il ne prend pas ses médicaments à l'heure ou qu'il ne les prend pas du tout
* Avertir le patient et le soignant que le distributeur est vide ou presque
* Avoir des médicaments de taille variable

## Objectifs
Concevoir et réaliser un distributeur de médicaments portable et modulaire avec un système d'authentification du patient qui l'avertit de l'heure de la prise de ses médicaments et qui avertit ses proches et, ou son soignant lorsqu'il est en retard sur la prise de ses médicaments.

## Produits existants

### [Spencer](https://spencerhealthsolutions.com/)
[![Spencer](https://i1.wp.com/spencerhealthsolutions.com/wp-content/uploads/2019/04/header_home.jpg?resize=1024%2C489&ssl=1)](https://spencerhealthsolutions.com/)

* Prix: indisponible
* Semble être destiné aux pharmacies
* Destiné à l'usage à domicile
* L'appareil rappel au patient de prendre ses médicaments
* Écran tactile
* Contient une réserve d'une semaine de médicaments
* Se recharge avec des cartouches sur lesquels se trouvent un code barre qui contient des informations sur l'heure à laquelle les médicaments doivent être distribués.
* Se connecte à d'autres appareils de santé
* Collecte des informations sur l’assiduité de la prise de médicaments pour l'envoyer au soignant
* Selle les médicaments dans de petits sacs en plastique avant de les distribuer

### [Hero](https://herohealth.com/)
[![Hero](https://herohealth.cdn.prismic.io/herohealth/65dc4cf64e545108561ed0bbcf4e87d8f7bf8279_img-device-lg2x.jpg)](https://herohealth.com/)

* Prix: 399$US
* Destiné à l'usage à domicile
* Écran non tactile
* Boutons de navigations
* Connecté au WiFi
* Application mobile et site web utilisée par le patient pour la configuration du Hero
* L'application mobile rappelle au patient de prendre ses médicaments
* Permet d'emmagasiner 10 médicaments différents
* La date d'expiration de chaque médicaments peut être configuré dans l'appareil
* Sert les médicaments dans un gobelet

### [Distributeur manuel](https://www.thingiverse.com/thing:3448180)

[![Distributeur manuel](https://cdn.thingiverse.com/renders/50/ba/5f/37/54/776a9c46f50f4bc53555d718d58695ec_preview_featured.JPG)](https://youtu.be/zVju94atrzQ "One Handed Pill Dispenser")

* Distribue un médicament tous les 1/4 de rotation
* Peut être utilisé à une seule main
* Modèle 3D sous licence CC disponible

<!--
### [9 distributeurs de médicaments](https://wiki.ezvid.com/best-smart-pill-dispensers)
**TODO**
-->

### Qu'est-ce que ces solutions n'ont pas?
* Portabilité
* Système d'authentification du patient

## Fonctionnalités
Cette section présente les fonctionnalités de ce projet accompagnées du besoin des clients qu'elles satisfont.

* Rappeler au patient de prendre ses médicaments à la bonne heure:
    * On peut configurer l'heure à laquelle les médicaments doivent être pris
    * A une alarme sonore (dont le volume peut être ajusté) pour rappeler l'heure de la prise de médicaments
    * Il y a un indicateur visuel pour montrer que l'utilisateur peut prendre sa prescription
    * Envoi des messages textes au patient pour qu'il prenne son médicament

* Empêcher le patient de prendre plusieurs fois sa prescription
    * Mécanisme de verrouillage du distributeur qui se débloque lorsque le patient doit prendre sa dose et s'est authentifié et se rebloque après

* Empêcher l'accès aux médicaments aux personnes autres que le patient
    * Mécanisme de verrouillage du distributeur
    * Système d'authentification du patient
    * Empêche le distributeur d'être déverrouillé sans un code

* Avertir la famille et, ou le soignant du patient lorsqu'il ne prend pas ses médicaments à l'heure ou qu'il ne les prend pas du tout
    * Envoie un message au soignant lorsque le patient a un retard de `N` minutes ou plus pour sa prise de médicaments. Envoie un autre message lorsqu'il l'a finalement pris
    * Il affiche une distribution du retard de prise de médicaments pour le mois, la semaine, l'année, etc. ![distribution](./images/distribution.png)

* Avertir le patient et le soignant lorsque le distributeur est vide ou presque
    * Le nombre de médicaments mis dans l'appareil est retenu par l'appareil et le nombre consommé est déduit pour savoir combien de médicaments il reste.
    * Le distributeur détecte qu'il ne reste plus de médicaments (au cas où le mauvais nombre de médicaments ait été entré)
    * Indicateur visuel que le distributeur est vide
    * Envoi un message au soignant pour l'avertir que le distributeur est vide.

* Avoir des médicaments de tailles variables
    * Le mécanisme de distribution s'adapte à différentes tailles de médicaments

* Donner les bons médicaments aux bons patients
    * Système d'authentification des patients

* Autres
    * Modulaire: le système est composé de modules qu'on peut combiner ensemble. Chaque module contient son type de médicament
    * Application mobile pour le patient, ses proches et ses soignants Cross Platform (Android et iOS)

\* Optionnel

## Contraintes
* Portable:
    * Dimensions d'un module: 10cm x 10cm x 10cm
    * Peut être déplacé facilement dans un autre endroit sans configurations supplémentaires
    * Utilise le réseau cellulaire (GSM)
* Modulaire:
    * Supporter au moins 5 modules de médicaments connectés
    * Limiter le nombre de fils pour interconnecter les modules
    * Liaison physique solide entre les modules, pour ne pas qu'ils ne se déconnectent facilement
    * Pouvoir reconnecter les modules qui se déconnectent intentionnellement ou par accident sans aucune configuration requise (Hot plug)
* Utiliser une plateforme Cross-Platform pour l'application mobile.  
* Langages de programmation:
    * C/C++ pour le module de contrôle
    * C pour les modules de distribution
* Système d'authentification biométrique. Permet aux personnes avec des troubles de la mémoire de ne pas avoir à se rappeler d'un mot de passe ou de garder un dispositif supplémentaire (ex.: clé)
* Fonctionne même si le client n'a pas de téléphone cellulaire
* Utilise un matériau sécuritaire pour la nourriture

## Livrables
* Distributeur de médicaments
* Application mobile pour le patient et le soignant
* Manuel de l'usager expliquant:
    * Configuration du distributeur et de l'application
    * Utilisation du distributeur
    * Utilisation de l'application pour les patients, les proches et les soignants

## Échéancier
Date de début: 10 février 2020  
Date de fin: 12e~13e semaine de la session d'hiver 2020  
**À préciser au début de la session d'hiver 2020**

## Problèmes
* On peut savoir que le patient a pris ses médicaments du distributeur, mais on ne sait toujours pas s'il les a consommé.
* Les puces GSM ne supportent pas tous les fournisseurs de services mobiles dans toutes les régions.
* Qui posséderait l'appareil. Serait-il acheter par les patients et leurs proches ou leur serait il loué par les pharmacies.
