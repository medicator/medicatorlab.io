# Conclusion

## Pièces

### Lecteur d'empreintes digitales
Utiliser mon deuxième choix, car il consomme tellement moins d'électricité

## Fonctionnalités à ajouter

### Support pour plusieurs patients
Le même module de contrôle pourrait permettre à plusieurs patients de s'authentifier et de recevoir des doses différentes. Ceci serait particulièrement utile pour les maisons de personnes âgées et pour les couples. Dans les maisons de personnes âgées, beaucoup de distributeurs de médicaments pourraient être connectés au même module de contrôle et les patients passeraient un à un pour prendre leurs médicaments.

### Le rendre portatif


## 3D
* Miniaturiser le tout
* Plus petits connecteurs
* Pouvoir relier les modules en les emboîtant plutôt qu'en utilisant un câble
* Design circulaire plutôt que linéaire
* S'inspirer du mécanisme des machines à bonbons?
* Trous de montage pour le PCB

