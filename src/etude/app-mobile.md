# Choix de la plateforme pour l'application mobile
Comme mentionné dans le cahier des charges, j'aimerais que l'application mobile soit compatible avec [Android] et [iOS].

Pour ce faire, j'ai deux grandes catégories de possibilités. La première est de développer une application native pour [Android] et une autre pour [iOS]. La deuxième, et la meilleure, est d'utiliser un écosystème qui permet d'utiliser le même code pour générer une application [iOS] et [Android].  
Le problème avec la première est que je n'ai pas d'ordinateur Apple sur lequel je pourrais faire le développement d'une application [iOS]. De plus, faire deux applications prendrait près de 2x plus de temps.

C'est pour cela que je vais dévier de mon cahier des charges et je vais envisager d'utiliser des écosystèmes qui ne fonctionnent que pour [Android] en plus des systèmes cross-platform.

## [Java] et [Kotlin]
[Java] et [Kotlin] sont deux languages de programmation permettant d'écrire des applications natives pour [Android] avec [Android Studio]. Ces deux languages sont 100% compatibles et peuvent tous les deux être utilisés dans la même application sans aucun problème.

J'ai de l'expérience avec ces deux languages et à faire des applications [Android] avec. Ils me permettront assurément de réaliser ce que je veux faire, mais ils ne me permettront pas de faire une application cross-platform.

Je préfère largement le [Java] au [Kotlin], car c'est un language fonctionnel plus récent qui permet d'écrire du code beaucoup plus concis.

## [C#] avec [Xamarin]
[C#] est le clone de [Java] fait par Microsoft. [Xamarin] est un écosystème cross-platform racheté par Microsoft utilisant [C#] avec lequel j'ai un peu d'expérience. Il permet de développer des applications pour [Android], [iOS] et UWP.

En plus de réutiliser le même code pour plusieurs applications, on peu écrire du code fait spécifiquement pour une plateforme ou une autre à l'aide de shared libraries.

Pour l'utiliser il est nécessaire d'avoir [Visual Studio]. En plus de demander beaucoup d'espace de disque, de mémoire vive et de puissance de processeur, [Visual Studio] n'est disponible que sur [MacOS] et [Windows] et j'aimerai pouvoir faire tout mon développement sur Linux.

## [JavaScript] ou [TypeScript]
Il existe beaucoup d'écosystèmes permettant d'écrire des applications cross-platform avec un de ces languages dont [NativeScript](https://www.nativescript.org/), [React Native](https://facebook.github.io/react-native/), [Cordova](https://cordova.apache.org/), [Ionic](https://ionicframework.com/), [Titanium](https://www.appcelerator.com/mobile-app-development-products/), [Ratchet](http://goratchet.com/), [Mobile Angular UI](http://mobileangularui.com/). Malheureusement, je n'ai aucune expérience avec ces écosystèmes, mais j'ai de l'expérience avec [React].

Le problème avec [JavaScript] est que ce n'est pas un langage typé, donc beaucoup de problèmes peuvent survenir au moment de l'exécution car les types des paramètres passés à certaines fonctions ne seront pas compatibles.

Pour sa part, [TypeScript] est du [JavaScript] avec des types. [TypeScript] est accompagné d'un transpileur qui permet de vérifier que tous les types dans le code sont valides, puis converti le code en [JavaScript].

# [Dart] avec [Flutter]
[Dart] est un language assez récent développé par Google. Il a la particularité d'avoir 4 compilateurs. Ils permettent de compiler en [JavaScript], en un format similaire au [[Java] Bytecode](https://en.wikipedia.org/wiki/Ahead-of-time_compilation) et en un exécutable natif.

[Flutter] est un nouvel écosystème cross-platform qui ressemble beaucoup à [React], mais qui utilise [Dart] plutôt que [JavaScript] et [TypeScript]. L'interface d'utilisateur a donc un état qui est modifié par les interactions de l'utilisateur et, à chaque modification de l'état, les éléments affichés à l'écran sont recalculés. Cette manière de faire permet de simplifier énormément la logique et de s'assurer que l'affichage est toujours consistent avec l'état.  

J'ai déjà de l'expérience avec [Flutter] dans [Android Studio] et les outils pour l'utiliser fonctionnent très bien. En plus de pouvoir réutiliser le même code pour l'application [Android] et [iOS], je peux écrire du code en [Java] et en Objective-C pour que je puisse ensuite l'utiliser dans mon code en [Dart] pour me donner accès à des APIs natifs de [Android] et [iOS] qui ne sont pas encore disponibles dans [Flutter]. Ceci ressemble beaucoup aux shared libraries dans [Xamarin].

## Conclusion
Comme je n'ai pas de machine [MacOS] pour compiler mon application pour [iOS] ou de iPhone pour la tester, je vais choisir un écosystème qui me permettra peut-être dans le futur de rendre mon application cross-platform, même si je ne pourrais pas le tester tout de suite.

J'ai donc choisi d'utiliser [Flutter], car j'aime beaucoup son fonctionnement et son language, qu'il est cross-platform et qu'il me permet de créer mes propres interfaces pour accéder à des APIs natifs d'[iOS] et [Android].

[Java]: https://www.java.com/en/
[Kotlin]: https://kotlinlang.org/
[Android]: https://www.android.com/
[iOS]: https://www.apple.com/iOS/
[MacOS]: https://www.apple.com/MacOS/
[Windows]: https://www.microsoft.com/windows
[JavaScript]: https://en.wikipedia.org/wiki/JavaScript
[TypeScript]: https://www.typescriptlang.org/
[Dart]: https://dart.dev/
[Flutter]: https://flutter.dev/
[React]: https://reactjs.org/
[Xamarin]: https://xamarin.com
[C#]: https://docs.microsoft.com/dotnet/csharp/
[Android Studio]: https://developer.android.com/studio/
[Visual Studio]: https://visualstudio.microsoft.com/
