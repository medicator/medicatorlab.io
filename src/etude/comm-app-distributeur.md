# Communications entre le distributeur et l'application
Pour l'interface entre l'application mobile et le distributeur, nous allons utiliser le réseau GSM, cependant, nous avons encore le choix d'utiliser des SMS ou l'Internet par le réseau GSM.

## SMS
```txt
(distributeur) <=> (serveur du fournisseur mobile) <=> (application mobile)
               GSM                                 GSM
```

### Avantages
* Les forfaits textos illimités coûtent moins cher que les forfaits avec des données
* Il n'est pas nécessaire de programmer de système pour enregistrer les messages si jamais un des appareil dans la communication n'est pas en ligne.

### Désavantages
* Il sera beaucoup plus difficile pour l'application et le distributeur que leurs messages ont étés délivrés, cependant ça devrait être possible.

## Internet 1
```txt
(distributeur) <=> (tout appareil pouvant se connecter à Internet)
      GSM, Ethernet, WiFi
```

### Avantages
* Le distributeur pourrait envoyer des messages à n'importe quel appareil pouvant se connecter à l'Internet.

### Désavantages
* Il sera pratiquement impossible de réaliser ce système à cause du NAT qui se trouve sur presque tous les réseaux.
* Il n'y a aucun moyen pour le distributeur et son interlocuteur de savoir l'adresse de l'autre.

## Internet 2
```txt
(distributeur) <=> (serveur central) <=> (tout appareil pouvant se connecter à Internet)
      GSM, Ethernet, WiFi      GSM, Ethernet, WiFi
```

### Avantages
* Le distributeur pourrait envoyer des messages à n'importe quel appareil pouvant se connecter à l'Internet.

### Désavantages
* Il faut programmer un serveur central.

## Conclusion
J'ai choisi d'utiliser le premier choix, car il est plus abordable et qu'il nécessite moins d'infrastructure. Le fait de ne pas avoir de serveur intermédiaire est un gros avantage, car on a pas besoin d'en programmer. En plus, il ne sera pas nécessaire de prouver au ministère de la santé que les données sont entreposées de manière sécuritaire sur le serveur, il faudra simplement prouver que les données sont transférées de manière sécuritaire entre le distributeur et les téléphones mobiles et qu'elles sont entreposées de manière sécuritaire sur les téléphones mobiles.
