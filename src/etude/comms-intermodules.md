# Communications inter-modules
Comme mentionné dans le cahier des charges, [I²C](https://en.wikipedia.org/wiki/I%C2%B2C) sera utilisé pour la communication entre le module contrôle et ceux de distribution.

Ce système de communication va devoir permettre la connexion et la déconnexion de modules de distribution au module de contrôle sans aucune configuration supplémentaire.

## Connecteur
Pour connecter les modules ensemble 4 fils sont nécessaires: 1 pour le 5V, 1 pour la masse, 1 pour le signal d'horloge et 1 pour les données.

J'aurais beaucoup aimé avoir un mécanisme élastique permettant de coller ensemble deux modules en poussant puis de les verrouiller en les tournant puis en arrêtant de forcer pour les coller. Ensuite, pour les déconnecter, il aurait fallut les forcer de nouveau sur les deux modules collés, puis les tournés dans le sens inverse et les séparer.

Comme il pourrait être dur de réaliser un mécanisme de ce genre et d'y intégrer un connecteur 4 broches, j'ai décidé de ne pas le faire. Les modules auront tout simplement un connecteur populaire avec 4 fils: USB. J'utiliserai des câbles USB-A à USB-A de n'importe quelle génération. Les câbles USB sont un bon connecteur dans ce cas, car ils ont au moins 4 conducteurs, 2 pour les données et 2 pour l'alimentation. Ils sont également abordables, durables (dans le cas de USB-A) et peuvent facilement être remplacés par les clients s'ils les brisent ou les perdent.  
Il faudra cependant mentionner aux clients que les modules ne sont pas faits pour être connectés à tout autre appareil, même s'ils ont un port USB.

## Assignation des adresses du bus I²C
* `0` à `8`: [Réservées](https://www.i2c-bus.org/addressing/).
* `8`: Adresse utilisée par les modules de distribution pour envoyer une demande pour une nouvelle adresse au module de contrôle.
* `9`: Adresse utilisée par les modules de distribution pour envoyer un message encrypté au module de contrôle.
* `10` à `119`: Adresses des modules de distribution.
* `120`: Adresse utilisée par le module de contrôle pour envoyer une nouvelle adresse I²C à un module de distribution.
* `121` à `127`: [Réservées](https://www.i2c-bus.org/addressing/).

## Connexion
Pour ce faire lorsqu'un module de distribution va se connecter à un module de contrôle, il va se donner la dernière adresse possible du bus I²C et commencer une communication avec la première adresse possible du bus I²C.

Le module de distribution va envoyer son identifiant unique au module de contrôle et le module de contrôle va lui répondre en lui renvoyant son identifiant unique ainsi qu'une nouvelle adresse I²C. Le module de distribution va alors reconfigurer son adresse I²C.

Lors de cette communication le dernier octet envoyé par chacun des modules est utilisé pour vérifier l'intégrité du message reçu.

## Messages envoyés du module de contrôle aux modules de distribution
Tous les messages envoyés du module de contrôle aux modules de distributions doivent être sécurisés pour empêcher quelqu'un de pouvoir se connecter au bus I²C et de pouvoir envoyer toutes les commandes qu'il veut.

C'est pour cela que toutes les données envoyées par le module de contrôle seront encryptée avec l'algorithme AES256 avec CBC, sauf l'octet d'intégrité et le vecteur d'initialisation.

Voici le contenu d'un message envoyé par le module de contrôle aux modules de distribution:
* [Vecteur d'initialisation](#vecteur-dinitialisation)
* [Code de la commande](#code-de-la-commande)
* [Code de confirmation](#code-de-confirmation)
* [Nouvelle clé d'encryption](#nouvelle-cle-dencryption)
* [Données de la commande](#donnees-de-la-commande)
* [Octet d'intégrité](#octet-dintegrite)

Voici le contenu d'une réponse retournée par un module de contrôle:
* [Code d'erreur](#code-derreur)
* [Code de confirmation](#code-de-confirmation)
* [Données de la réponse](#donnees-de-la-reponse)
* [Octet d'intégrité](#octet-dintegrite)

### Commandes
Voici la liste de toutes les commandes accompagnées. Cette liste contient le nom de chaque commande, leur description, leur code, leurs données et leur réponse.

#### Configurer
Permet de configurer le nombre de médicaments dans un module.

##### Requête
* Code: `0x00`
* Données: Nombre de médicaments, entier de 1 octet

##### Réponse
* Données: aucunes

#### Réinitialiser
Permet de réinitialiser le module, sa clé d'encryption et son nombre de médicaments.  
Il faudra envoyer la commande de configuration au module avant qu'il n'accepte de répondre à toute autre commande.

##### Requête
* Code: `0x01`
* Données: aucunes
* Clé d'encryption: __ce paramètre est ignoré pour cette commande__

##### Réponse
* Données: aucunes

#### Distribuer `n` médicaments
Permet de demander au module de distribution de distribuer `n` médicaments.

##### Requête
* Code: `0x02`
* Données: Nombre de médicaments, entier de 1 octet

##### Réponse
* Données: aucunes

#### Déverrouiller la recharge pendant `n` secondes
Permet de déverrouiller le mécanisme pour la recharge de médicaments pendants `n` secondes.

##### Requête
* Code: `0x02`
* Données: Durée en secondes de déverrouillage de la recharge, entier de 1 octet

##### Réponse
* Données: aucunes

#### Obtenir le nombre de médicaments
Permet d'obtenir le nombre de médicaments restants dans le module.

##### Requête
* Code: `0x03`
* Données: aucunes

##### Réponse
* Données: nombre de médicaments, entier de 2 octets dont le premier est le plus significatif

#### Obtenir le nombre de médicaments en attente d'être distribués
Permet d'obtenir le nombre de médicaments en attente d'être distribués dans le module.

##### Requête
* Code: `0x04`
* Données: aucunes

##### Réponse
* Données: nombre de médicaments, entier de 2 octets dont le premier est le plus significatif

### Vecteur d'initialisation
Vecteur d'une longueur de <abbr title="à déterminer">???</abbr> octets générés de manière aléatoire utilisés pour initialiser les modules d'encryption et de décryption. Un vecteur constant pourrait être utilisé au lieu de communiquer un vecteur différent pour chaque communication, mais changer constamment ce nombre est plus sécuritaire.

Il n'est pas encrypté dans le message.

### Code de la commande
Les codes de commandes sont des entiers d'un seul octet qui sont envoyées par le module de contrôle pour informer le module de distribution de la signification du message. Les différents types de commandes peuvent être vus [ici](#commandes).

Il est encrypté dans le message.

### Code de confirmation
Le code de confirmation est un nombre aléatoire encrypté et envoyé par le module de contrôle. Le module de distribution va ensuite le décrypter et le renvoyer au module de contrôle pour que le module de contrôle s'assure qu'il a bel et bien envoyé la commande au bon module de contrôle.

### Nouvelle clé d'encryption
Chaque fois qu'une commande est envoyée par le module de contrôle à un module de distribution, il lui renvoi une nouvelle clé d'encryption. Ceci permet au module de contrôle de confirmer que c'est bel et bien le module de distribution qui a reçu le message et non un attaquant.  

Cette clé d'encryption est encryptée avec l'ancienne.

### Données de la commande
Ce sont les données envoyées par le module de distribution.

### Octet d'intégrité
Un octet d'intégrité est envoyé à la fin de chaque requête et de chaque réponse.  
Il n'est pas encrypté et il est la somme tous les octets de la communication avant qu'ils ne soient encryptés.

### Code d'erreur
Le code d'erreur est le premier octet envoyé dans les réponses des modules de distribution.  
Voici la liste de ses valeurs possibles:
* `0`: Aucune erreur
* `1`: Le module est déjà configuré
* `2`: Le module n'est pas encore configuré
* `3`: Les données reçues étaient corrompues
* `4`: Les données reçues n'ont pas pues être décryptées
* `6`: La code de la commande reçue est inconnu
* `7`: La quantité de données reçues est supérieur à celle permise
* `8`: Le distributeur ne contient plus assez de médicaments pour en distribuer

### Données de la réponse
Les données de la réponse varient d'une commande à l'autre et ne sont pas encryptées.

### Algorithme du module de contrôle
```txt
Début du pseudo-code de la fonction int envoyerCommande(uint32_t uIdModule, uint8_t uCommande, char *sDonnees, uint16_t uTailleDonnees, char *sDonneesReponse, uint16_t uTailleDonneesReponse)

Initialiser un tampon.

Générer un vecteur d'initialisation aléatoire.
Ajouter le vecteur d'initialisation au tampon.

Générer une nouvelle clé d'encryption.
Ajouter la clé d'encryption au tampon.

Générer un code de confirmation.
Ajouter le code de confirmation au tampon.

Ajouter le code de la commande au tampon.
Ajouter les données au tampon.

Initialiser un octet d'intégrité à 0.
Ajouter chaque octet du tampon à l'octet d'intégrité.

Obtenir la clé d'encryption associée au module donné.
Obtenir l'adresse I²C du module donné.

Initialiser le module d'encryption avec le vecteur d'initialisation.
Encrypter toutes les données du tampon.

Ajouter l'octet d'intégrité à la fin du tampon.

Envoyer tout le tampon à l'adresse de destination.
Attendre un certain délai pour laisser le temps au module de distribution de préparer sa réponse.
Lire la réponse du module de distribution.
Vérifier l'intégrité du message.

Si (aucune erreur n'est survenue et que le code de confirmation retourné est le même que celui envoyé) alors
    Enregistrer la nouvelle clé d'encryption.
Fin du si

Retourner le code d'erreur reçu à la routine appelante.

Fin du pseudo-code de la fonction int envoyerCommande(uint32_t uIdModule, uint8_t uCommande, char *sDonnees, uint16_t uTailleDonnees, char *sDonneesReponse, uint16_t uTailleDonneesReponse)
```

### Algorithme du module de distribution
```txt
Début du pseudo-code de la fonction int recevoirCommande()

Initialiser un tampon.

Recevoir toutes les données par I²C dans le tampon.
Initialiser un octet d'intégrité à la valeur du dernier octet du tampon.

Initialiser le module de décryption avec le vecteur d'initialisation reçu dans le tampon.

Obtenir la clé d'encryption de ce module.
Configurer la clé d'encryption pour le module de décryption.

Décrypter les données du tampon.
Verifier l'intégrité des données.

---
Traiter les données.
---

Attendre que le module de contrôle demande à avoir une réponse.
Envoyer la réponse au module de contrôle.

Si (aucune erreur n'est survenue) alors
    Enregistrer la nouvelle clé d'encryption.
Fin du si

Retourner un code d'erreur à la routine appelante.

Fin du pseudo-code de la fonction int recevoirCommande()
```

# Problème
Il faut que le module de contrôle puisse signaler que les données reçues n'avait aucune intégrité.

## Messages envoyés du module de distribution au module de contrôle
Contrairement aux commandes envoyées par le module de contrôle, les messages envoyés par les modules de distribution ne sont pas aussi importants. Il n'est pas aussi grave que quelqu'un puisse fausser ces messages. La pire conséquence qui pourrait arriver est que quelqu'un pourrait pousser le module de contrôle à envoyer des notifications au patient, au soignant et au pharmacien sans aucune raison. Ce scénario est loin d'être catastrophique, car il permet au contraire d'avertir ces personnes que quelqu'un est en train d'essayer de déjouer le système.

## Nombre de médicaments 
