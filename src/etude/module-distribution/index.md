# Module de distribution

## Diagramme schématique
> ![Image Manquante](../../images/schematique-module-distribution.png)  
Diagramme schématique

#### Tableau des composants
![Image Manquante](../../images/tableau-comps-distribution.png)

#### Tableau des connecteurs
![Image Manquante](../../images/tableau-conns-distribution.png)

#### Tableau des alimentations
![Image Manquante](../../images/tableau-alims-distribution.png)

#### Tableau des derniers codes de référence
![Image Manquante](../../images/tableau-refsmax-distribution.png)

## Image 3D
<!--
> ![Image 3D](../../images/3d-module-controle.png)
-->

> ![Image 3D](https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png)

## Calculs

### Résistance de la LED rouge
\\( V_{cc} = 5V, V_f = 2V, I_f = 20mA \\)  
\\( R = \frac {V_{cc} - V_f}{I_f} \\)  
\\( R = \frac {5 - 2}{0.02} \\)  
\\( R = \frac {3}{0.02} \\)  
\\( R = 150 \Omega \\)  

### Résistance de la LED verte et de la bleue
\\( V_{cc} = 5V, V_f = 3.2V, I_f = 20mA \\)  
\\( R = \frac {V_{cc} - V_f}{I_f} \\)  
\\( R = \frac {5 - 3.2}{0.02} \\)  
\\( R = \frac {1.8}{0.02} \\)  
\\( R = 90 \Omega \\)  

### Résistance de l'émetteur du photo-interrupteur
\\( V_{cc} = 5V, V_f = 1.7V, I_f = 30mA \\)  
\\( R = \frac {V_{cc} - V_f}{I_f} \\)  
\\( R = \frac {5 - 1.7}{0.03} \\)  
\\( R = \frac {3.3}{0.03} \\)  
\\( R = 110 \Omega \\)  

### Résistance du récepteur du photo-interrupteur
\\( V_{cc} = 5V, V_{ce} = 0.4V, I_{ce} = 1mA \\)  
\\( R = \frac {V_{cc} - V_{ce}}{I_{ce}} \\)  
\\( R = \frac {5 - 0.4}{0.001} \\)  
\\( R = \frac {4.6}{0.001} \\)  
\\( R = 4600 \Omega \\)  
