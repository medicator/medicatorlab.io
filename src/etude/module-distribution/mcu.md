# Sélection du microcontrôleur

## Caractéristiques recherchées:
* Prix maximum: 10$
* Consommation énergétique: basse (< 25mA)
* 1 bus I²C
* USB pour la programmation et le débogage
<!--* Nb de GPIOs: ? **TODO**-->
* Nombre d'entrée analogiques: 2
* Encryption AES256
* Mémoire non volatile pour enregistrer sa configuration
* Facilement programmable
* Encryption: AES128 ou AES256

<!--
## Options envisagées

https://www.digikey.ca/products/en/integrated-circuits-ics/embedded-microcontrollers/685?k=I2C

* ATtiny
* PIC quelconque **TODO**
* [ATMEGA328P-PU](https://www.digikey.ca/product-detail/en/microchip-technology/ATMEGA328P-PU/ATMEGA328P-PU-ND).

<--
* PIC du département
* **Regarder les samples gratuits des compagnies et mentionner que c'est pour un projet final d'étude**
-->

## Choix
J'ai choisi le [ATMEGA328P-PU](https://www.digikey.ca/product-detail/en/microchip-technology/ATMEGA328P-PU/ATMEGA328P-PU-ND), car je suis persuadé qu'avec ce MCU j'aurai assez de mémoire vive et morte pour ne pas que ça me limite dans le développement.  
Je peux aussi utiliser l'IDE d'Arduino si je le veux et j'ai même déjà travaillé avec ce microcontrôleur, puisque c'est celui du Arduino UNO ce qui signifie que je pourrai utiliser le grand nombre de librairies faites pour le UNO.

Pour éviter d'ajouter un programmeur sur mon circuit, j'utiliserai un autre Arduino UNO faire la programmation de mon microcontrôleur comme expliqué dans [ce tutoriel d'Arduino](https://www.arduino.cc/en/Tutorial/ArduinoToBreadboard).  
Cependant, au lieu d'ajouter une horloge externe, j'utiliserai l'horloge interne du microcontrôleur, car je n'ai pas besoin que son horloge soit précise puisque je n'utiliserai que des interfaces de communication synchrone (I²C).

<!--
### Options
* [Recherche DigiKey](https://www.digikey.ca/products/en/integrated-circuits-ics/embedded-microcontrollers/685?k=I2C)
* [PIC Microchip](https://www.microchip.com/design-centers/8-bit/pic-mcus). Maxime recommande de ne pas utiliser ceux du département, car ils sont vieux.
-->
