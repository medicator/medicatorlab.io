# Mécanisme de distribution
Pour le mécanisme de distribution de médicaments, il est important qu'il puisse distribuer un médicament à la fois et qu'il ne puisse pas être forcé, même lorsque l'appareil est fermé. Il faut donc qu'il reste verrouillé lorsque l'alimentation est coupée.

Le premier mécanisme auquel j'ai pensé est d'utiliser un servo moteur pour déplacer une gâche, des capteurs de fin de course pour savoir que le servo moteur a fait le chemin et pour ne pas qu'il ne force pour rien et un solénoïde permettant de verrouillé le mécanisme en position fermé, même lorsqu'il n'y a pas d'alimentation.
<!--TODO [Image Manquante](../../images/distributeur-medoc-1.png)-->

Le deuxième système auquel j'ai pensé est d'utiliser un simple moteur, un pont en H, un mécanisme de transmission de mouvement irréversible et des capteurs de fin de course, pour ne pas qu'ils forcent pour rien.
<!--TODO[Image Manquante](../../images/distributeur-medoc-2.png)-->

J'ai choisi d'utiliser le deuxième système, car il est mécaniquement moins complexe et qu'il coûte moins cher, car il n'y a pas de solénoïde à acheter et qu'un simple moteur vaut moins qu'un servo moteur. Des moteurs beaucoup moins énergivores que des servo moteurs peuvent aussi être trouvés.

## Moteur
Ce système contient d'abord moins de composants que le précédent. De plus on peut trouver des moteurs qui consomment beaucoup moins énergivores.

### Contraintes
* Prix maximal: 15$
* Tension maximale: 5V
* Courant maximal: 150mA

<!--
### Recherches moteur
* <https://www.digikey.ca/products/en/motors-solenoids-driver-boards-modules/motors-ac-dc/178?FV=110%7C350448%2C110%7C365115%2C110%7C407938%2C183%7C337557%2C-8%7C178%2C14%7C1%2C14%7C169130%2C14%7C169425%2C14%7C169460%2C14%7C169469%2C14%7C169473%2C14%7C175229%2C14%7C178452%2C14%7C202627%2C14%7C203005%2C14%7C206189%2C14%7C208767%2C14%7C210265%2C14%7C210279%2C14%7C228829%2C14%7C249586%2C14%7C51272%2C14%7C55871&quantity=0&ColumnSort=1000011&page=1&pageSize=25>
* <https://www.mouser.ca/Search/Refine?Keyword=dc+motor>
* <https://www.newark.com/search?st=dc%20motor>
* <https://www.robotshop.com/en/catalogsearch/result/?q=dc+motor&order=relevance&dir=desc>
* <https://www.futureelectronics.com/search/?text=dc+motor>
* <https://www.alliedelec.com/view/search/?category=2%7C2510423%2F2520517&n10513=1%20W%2C0.087%20W%2C0.29%20W%2C0.45%20W%2C0.39%20W%2C0.62%20W%2C0.59%20W%2C0.58%20W%2C0.5%20W%2C0.46%20W%2C0.394%20W%2C0.85%20W%2C0.77%20W%2C0.87%20W%2C0.95%20W&sort=price_retailasc>
* <https://abra-electronics.com/?subcats=Y&pcode_from_q=Y&pshort=Y&pfull=Y&pname=Y&pkeywords=Y&search_performed=Y&q=5v+dc+motor&dispatch=products.search>
-->

### Options envisagées
Digikey:
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q4KL2BQ280001.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q4KL2BQ280001.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q4TL2BQ230001.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q4TL2BQ380001.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q4TL2BQ360003.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q6SL2BQ180002.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q4SL2AQ210005.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Z4SL2A0000003.pdf>
* <http://www.vibration-motor.com/wp-content/themes/vibration-motors/dk-pdf/products/download/Q4SL2BQ280001.pdf>
* <https://media.digikey.com/pdf/Data%20Sheets/Seeed%20Technology/108990001_Web.pdf>
* <https://www.kitronik.co.uk/2546-low-inertia-solar-motor-1000-rpm.html>

RobotShop:
* <https://www.robotshop.com/en/gm24-tiny-geared-motor-w-90-degree-shaft.html>
* <https://www.robotshop.com/en/gravity-dc-micro-metal-gear-motor-driver-301.html>
* <https://www.robotshop.com/en/makeblock-geared-motor-dc-6v-200rpm-mbot.html> ?
* <https://www.robotshop.com/en/r280-3-6v-12000-rpm-brushed-dc-motor.html> ?

### Choix
[Moteurs pour panneaux solaires 2546](https://www.kitronik.co.uk/2546-low-inertia-solar-motor-1000-rpm.html).  
Je pense que celui-ci sera le meilleur, car il tourne beaucoup moins vite que les autres (1820RPM), donc on aura moins besoin de réduire sa vitesse. De plus, il est utilisé pour faire avancer de petites voitures, donc je peux être sûr qu'il sera assez fort pour déplacer le mécanisme de distribution de médicaments.

## Capteur de fin de course
Pour le capteur de fin de course, presque n'importe lequel fera l'affaire. Il suffit qu'il puisse physiquement être actionné par le mécanisme et qu'il soit assez petit.

J'avais donc sélectionné le [MS0850503F010S1A](https://www.digikey.ca/product-detail/en/e-switch/MS0850503F010S1A/EG4544-ND/1628281), mais finalement j'utiliserai le [MSW-14](http://smparts.com/product_info.php?cPath=2_602&products_id=6691) qui est disponible au département.

<!--
#### Recherches limit switches (capteur de fin de course)
https://www.digikey.ca/products/en/switches/snap-action-limit-switches/198?k=&pkeyword=limit+switch&sv=0&sf=1&FV=258%7C333006%2C258%7C352076%2C258%7C387839%2C258%7C391477%2C258%7C391480%2C258%7C391482%2C258%7C391483%2C258%7C391488%2C258%7C391496%2C258%7C400555%2C258%7C405023%2C258%7C405075%2C258%7C405098%2C258%7C405100%2C258%7C405102%2C258%7C421432%2C258%7C421515%2C545%7C314781%2C545%7C364666%2C545%7C364667%2C545%7C364669%2C545%7C364670%2C545%7C364671%2C-8%7C198%2C550%7C103028%2C550%7C103029%2C550%7C122795%2C550%7C170340%2C550%7C175482%2C550%7C204287%2C550%7C229880%2C550%7C230308%2C550%7C230405%2C550%7C250547%2C550%7C250873%2C550%7C268333%2C550%7C268762%2C550%7C271967%2C550%7C282194%2C550%7C282533%2C550%7C283699%2C550%7C295925%2C550%7C296257%2C550%7C73359%2C550%7C87097%2C550%7C92261%2C550%7C96911&quantity=&ColumnSort=1000011&page=1&stock=1&pageSize=25
-->

<!--
#### Choix
https://www.digikey.ca/product-detail/en/e-switch/MS0850503F010S1A/EG4544-ND/1628281
-->
