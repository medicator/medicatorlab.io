# Détecter que le distributeur est vide

## Besoins du client
Le client veut être avertit lorsqu'il n'a **presque plus** de médicaments afin de pouvoir aller en re-commander chez son pharmacien.

Si le client doit prendre un médicament le matin et un autre le soir, il faut qu'il puisse prendre son médicament le soir, être avertit qu'il ne lui en reste presque plus, mais qu'il lui en reste assez pour qu'il puisse se lever le lendemain et prendre sa dose.

## Solution
Demander au client d'entrer le nombre de médicaments dans le distributeur lorsqu'il le recharge. On va ensuite compter le nombre de médicaments qu'il reste au fur et à mesure qu'ils sont pris par le client.  
Puisque le client pourrait faire une erreur, il faudra aussi ajouter des capteurs qui nous avertiront que le distributeur est **presque vide**.  

### Technologie de capteur: Lumière vs Ultrason
Deux technologies s'offrent à nous: les ultrasons et la lumière.  
Les ultrasons risquent cependant de rebondir sur les parois de l'entonnoir et être déclenchés alors qu'il y a des médicaments entre l'émetteur et le récepteur.  
Comme la surface des parois et les médicaments ne devraient pas réfléchir la lumière, on utilisera un capteur utilisant la lumière.

### Disposition des capteurs
Les capteurs seront placés dans le col de l'entonnoir. Comme ceci:

Puisque les médicaments pourront avoir des tailles différentes, il est possible que les faisceaux des capteurs passent parfois entre deux médicaments. Afin d'éviter cela.

### [Through-Beam vs Retro-Reflective vs Diffuse Reflection](https://en.wikipedia.org/wiki/Photoelectric_sensor#Difference_between_modes)
Comme les capteurs seront placés assez proches les uns des autres, il faut s'assurer qu'ils ne se causent pas d'interférences, donc j'utiliserai le type de capteur photosensible le plus précis: through-beam.

### Émetteur récepteur Tout-en-un vs Séparé
L'émetteur récepteur du capteur sont disponibles en deux formats: tout-en-un ou séparé.

Ceux qui sont tout en un laissent un espace ou placer l'objet qui va bloquer l'émetteur, mais cet espace est fixe. Comme le boîtier n'est pas encore fait et que de toutes les façons il pourrait changé, il vaut mieux utiliser des émetteurs et récepteurs séparés qu'on peut placer à n'importe quelle distance.

### Choix du capteur
#### Sommaire des restrictions
* Type: Capteur Photoelectric through-beam
* Émetteur et récepteur séparé
* Distance: 5mm-20mm ou moins
* Tension: 3.3~5V
* Prix: <20$

#### Recherches
https://www.digikey.ca/products/en/sensors-transducers/optical-sensors-photoelectric-industrial/562?FV=-8%7C562&quantity=0&ColumnSort=1000011&page=1&k=through-beam&pageSize=25

#### Choix
##### [2168](https://www.digikey.ca/product-detail/en/adafruit-industries-llc/2168/1528-2526-ND/8258463)
Prix: 9.25$  
Feuille du manufacturier: https://media.digikey.com/pdf/Data%20Sheets/Adafruit%20PDFs/2168_Web.pdf  
Avantages:
* Adapté à ne pas être placé sur un PCB

##### [TCZT8020-PAER](https://www.mouser.ca/ProductDetail/Vishay-Semiconductors/TCZT8020-PAER?qs=sGAEpiMZZMtIHXa%252BTo%2Fr2fyIABkR8cqU)
Prix: 1.90$  
Feuille du manufacturier: https://www.mouser.ca/datasheet/2/427/tczt8020-279874.pdf  
Avantages:
* Prix
* Taille

#### Conclusion
Je vais utiliser le **TCZT8020-PAER**, car sa taille me permettra de placer les deux capteurs plus près l'un de autre.
