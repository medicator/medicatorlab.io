# Servo pour distribuer les médicaments
Je vais garder ceci au cas où ce me soit utile plus tard, mais j'en doute.

## Choix

### [HS-422 (SER0002)](https://www.robotshop.com/en/hitec-hs-422-servo-motor.html)
https://www.robotshop.com/media/files/pdf/hs422-31422s.pdf
12.99$US

### [DFRobot Micro Servo Motor (SER0006)](https://www.robotshop.com/en/dfrobot-micro-servo-motor.html)
* Feuille du manufacturier: https://media.digikey.com/pdf/Data%20Sheets/DFRobot%20PDFs/SER0006_Web.pdf
* Prix: 4.50$US

### [HS-225MG High Performance Mini Servo](https://www.robotshop.com/en/hs-225mg-high-performance-mini-servo.html)
* Aucune feuille du manufacturier
* Prix: 25.99$USD

### [HS-55](https://www.robotshop.com/en/hitec-hs-55-micro-servo-motor.html)
https://www.robotshop.com/media/files/pdf/hs55.pdf
* 11.49$USD

## Conclusion
Je vais prendre le SER0006, car il est petit, abordable et disponible au magasin du département.
