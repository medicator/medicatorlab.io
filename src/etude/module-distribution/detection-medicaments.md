# Détection du niveau de médicaments
Il est très important que le patient ne manque jamais de médicaments. C'est pour cela qu'il sera possible d'entrer le nombre de médicaments présents dans le distributeur une fois qu'il sera rechargé. Il est cependant possible de faire des erreurs lors de la recharge, c'est pour cela qu'il serait intéressant d'avoir un mécanisme de sécurité qui permet de savoir qu'il ne reste presque plus de médicaments dans le distributeur sans se fier au compte qui pourrait être erroné. Il est important que ce mécanisme puisse détecter qu'il reste peu de médicaments assez longtemps en avance pour qu'un soignant puisse venir lui en redonner.

Comme on cherche pas à avoir un nombre précis de médicaments, il n'est pas nécessaire d'utiliser un capteur de poids ou de niveau, de simples photo-interrupteurs devraient suffire.

> ![Image Manquante](../../images/1-photo-interrupt.jpg)  
Distributeur avec plusieurs photo-interrupteurs

Comme le montre l'image ci-haut, avoir 1 seul photo-interrupteur pourrait être un problème, car le photo-interrupteur pourrait se trouver entre 2 médicaments et penser que le distributeur est presque vide alors que ce n'est pas le cas.  
C'est pour cela que j'utiliserai plusieurs photo-interrupteurs afin de diminuer les chances que ça arrive, comme le montre le schéma ci-bas. Pour ce prototype, j'en utiliserai exactement 2, mais il révélera peut-être que plus sont nécessaires pour éviter les fausses alertes.

> ![Image Manquante](../../images/2-photo-interrupt.jpg)  
Distributeur avec plusieurs photo-interrupteurs

## Choix du capteur
Comme plusieurs capteurs seront placés les uns à côtés des autres, il est important qu'ils n'interfèrent pas les uns avec les autres, c'est pour cela que les capteurs avec un laser diffus, comme ceux utilisant la reflection, ne pourront pas être utilisés.

Comme je ne sais pas encore quelle sera la taille du tube dans lequel passeront les médicaments et où se trouveront les capteurs, il faut que le capteur et l'émetteur puissent être placés à n'importe quelle distance l'un de l'autre. C'est pour cela que j'utiliserai un capteur et un émetteur séparés. Je sais cependant que cette distance sera inférieure à 5cm.
<!--https://www.digikey.ca/products/en/sensors-transducers/optical-sensors-reflective-analog-output/546-->

Heureusement pour moi, le magasin du département possède un couple capteur-émetteur avec ces caractéristiques, le [H23A1](../../annexe/H23A1.pdf), que j'utiliserai. Je test le fonctionnement de ce composant [ici](../../tests/capteur-medicament.md).
