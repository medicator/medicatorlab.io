# Outils pour PIC de microchip

## [IDE de Microchip](https://www.microchip.com/mplab/mplab-x-ide)
Gratuit

## [Encryption pour PIC18](http://ww1.microchip.com/downloads/en/AppNotes/00953a.pdf)
https://www.microchip.com/SWLibraryWeb/product.aspx?product=Encryption%20Routines

## Émulateur de PIC Microchip
```txt
community/gpsim 0.31.0-1 (1.3 MiB 6.9 MiB)
    Full-featured software simulator for Microchip PIC microcontrollers
```

## [Piklab](http://piklab.sourceforge.net/)
IDE open source pour PIC

## [GPUTILS](https://sourceforge.net/projects/gputils/)
Pas très téléchargé  
[tutoriel](https://hackaday.com/2010/11/03/how-to-program-pics-using-linux/)

## sdcc: Compilateur Open Source `pacman -S sdcc`
Compilateur open source pour PIC
