# IDE à utiliser
Plusieurs environnements de développement sont disponibles pour faire la programmation du ESP32.

## [Arduino IDE](https://www.arduino.cc/en/Main/Softwares)
[Voir ici](../module-controle/#arduino-ide)

## [MPLAB X IDE](https://www.microchip.com/mplab/mplab-x-ide)

#### Avantages
* Disponible sous Linux, Mac et Windows
* Permet de voir l'état de toutes les broches
* Configuration facile des registres

#### Désavantages
* Ne permet pas d'utiliser toutes les librairies d'Arduino

## [PlatformIO avec Arduino](https://docs.platformio.org/en/latest/boards/atmelavr/ATmega328P.html)
Voir [ici](../module-controle/ide.md#avantages-2) et [ici](../module-controle/ide.md#arduino)

## Choix
Parce que j'apprécierai vraiment d'avoir accès aux libraries d'Arduino et que c'est en partie pour cela que j'ai choisi le ATMEGA328P, je ne vais pas utiliser MPLAB X IDE.

J'ai donc choisi d'utiliser PlatformIO avec Arduino pour les mêmes raisons que j'ai choisi cela pour mon ESP32.
