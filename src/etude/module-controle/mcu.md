# Sélection du microcontrôleur

## Caractéristiques recherchées
* Prix maximal: 30$
* Interfaces:
    * Au moins 1 port I²C
    * Au moins 1 port UART
    * GSM
    * WiFi ou Bluetooth
<!--* Nb de GPIOs: ? **TODO**-->
* Encryption AES256
* Mémoire non volatile pour enregistrer sa configuration
* Facile à programmer

2 catégorise d'options s'offrent à nous:
* avoir un seul microcontrôleur avec le GSM intégré;
* avoir 2 microcontrôleur dont 1 préprogrammé et le GSM d'intégré et l'autre qui peut être programmé et est connecté au premier par une interface comme UART.

### Microcontrôleurs avec GSM
* [Sierra wireless](https://www.sierrawireless.com/products-and-solutions/embedded-solutions/iot-modules/)
* [Qualcomm](https://www.qualcomm.com/)
* [Simcom](https://simcom.ee/)
* [ESP32 avec SIM800L](https://www.banggood.com/LILYGO-TTGO-T-Call-V1_3-ESP32-Wireless-Module-GPRS-Antenna-SIM-Card-SIM800L-Board-p-1527048.html?rmmds=buy&cur_warehouse=CN)
* [Adafruit Feather 32u4 FONA](https://www.adafruit.com/product/3027)

### Microcontrôleurs sans GSM
* [AT89C2051](https://www.microchip.com/wwwproducts/en/AT89C2051)
* [GSM](https://www.electronicshub.org/gsm-interfacing-8051-microcontroller/)

### Microcontrôleurs préprogrammés avec GSM
* [Adafruit FONA 800, 808 ou 3G](https://www.adafruit.com/product/2542), le 800 a toutes les fonctionnalités nécessaires
* [SIM9000D](https://microcontrollerslab.com/gsm-module-interfacing-with-microcontroller)
* [SIM800L GPRS GSM Module MicroSIM Card Core Board Quad-band TTL Serial Port](https://www.aliexpress.com/item/32708504554.html?scm=1007.22893.149155.0&pvid=21221d4e-45fd-4963-90cd-17bbad826c47&onelink_page_from=ITEM_DETAIL&onelink_item_to=32708504554&onelink_duration=0.830112&onelink_status=noneresult&onelink_item_from=32708504554&onelink_page_to=ITEM_DETAIL&aff_platform=product&cpt=1571788387662&sk=BIu72rz&aff_trace_key=090e908297ba4afbb1a446627a5f3a13-1571788387662-06902-BIu72rz&terminal_id=43e4ce26a06d4a71a598472f1c1aca25) ([digikey](https://www.digikey.ca/products/en?keywords=SIM800L))

## Conclusion
J'ai finalement choisi le ESP32 avec le SIM800L qui sont deux microcontrôleurs. Le ESP32 est bi-coeur, possède plus de 30 GPIOs, du WiFi et du Bluetooth et c'est celui que je programmerai. Le SIM800L est le microcontrôleur préprogrammé qui permet de se connecter au réseau GSM. Ils ont tous les deux été placés sur un circuit imprimé d'un format similaire au NodeMCU du nom de TTGO T-Call V1.3 ESP32 et sont reliés par UART. Il a aussi l'avantage de pouvoir être programmé avec l'interface d'Arduino et d'utiliser plusieurs libraries existantes pour utiliser le SIM800L.

![Image Manquante](../../images/ttgo-tcall.jpg)
