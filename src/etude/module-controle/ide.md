# IDE à utiliser
Plusieurs environnements de développement sont disponibles pour faire la programmation du ESP32.

## [Arduino IDE](https://www.arduino.cc/en/Main/Softwares)

#### Avantages
* Disponible sur Linux, Mac et Windows
* J'ai de l'expérience avec
* Il est très utilisé
* Beaucoup de libraries sont disponibles
* Pas besoin de Makefiles

#### Désavantages
* Aucune auto-complétion
* Gère mal les dépendances: les dépendances des libraries téléchargées ne sont pas aussi téléchargées

## [ESP IDF](https://docs.espressif.com/projects/esp-idf/en/stable/)

#### Avantages
* Disponible sur Linux, Mac et Windows

#### Désavantages
* Moins populaire que Arduino
* Ne permet pas de facilement gérer le dépendances
* N'a pas d'IDE dédié
* Nécessite d'écrire ses propres Makefiles
* Je n'ai aucune expérience avec ce logiciel

## [PlatformIO](https://platformio.org/)

#### Avantages
* C'est un plugin pour VisualStudio Code et Atom
* Il est donc disponible sur Linux, Mac et Windows
* Offre une très bonne auto-complétion
* Meilleure structure de dossiers que Arduino
* Surligne les erreurs de compilation directement dans les fichiers source du code
* Système de gestion des dépendances pour chaque projet
* Beaucoup des librairies d'Arduino sont disponibles sur cette plateforme et celles qui ne le sont pas peuvent quand même être ajoutées au projet
* Permet d'écrire des tests et de les exécuter automatiquement sur un appareil
* Meilleure documentation technique qu'Arduino
* Pas besoin d'écrire de Makefiles
* Permet de configurer un taux de baud par défaut du terminal pour chaque projet

#### Désavantages
* Moins populaire que Arduino
* Le plugin pour VisualStudio Code plante parfois, mais c'est celui qui est recommander par PlatformIO
* Je l'ai très peu utilisé

### Arduino

#### Avantages
* Je pourrais utiliser des librairies d'Arduino

### ESP IDF

#### Avantages
* Aucun

#### Désavantages
* Moins populaire

### [Liste complète](https://en.wikipedia.org/wiki/ESP32#Programming)

## Choix
Mis appart mon manque d'expériences avec PlatformIO je pense que ça vaut définitivement la peine de l'utiliser et que ses fonctionnalités vont me permettre de programmer plus facilement et rapidement.

Il ne vaut pas la peine d'utiliser ESP IDF par lui-même puisqu'on doit nous-même gérer les dépendances et qu'il ne vient pas avec un éditeur. Donc si je choisissais cette plateforme j'utiliserais PlatformIO avec. Je ne pense cependant pas qu'il vaille la peine de faire cela, car la librairie Arduino pour le ESP32 donne aussi un accès directe aux libraries de ESP IDF.

Je vais donc utiliser PlatformIO avec Arduino et si j'expérimente trop de problèmes d'éditeur qui plante j'utiliserais le Arduino IDE à la place, puisque je sais que je n'ai qu'à changer l'emplacement des fichiers de code pour que tout fonctionne.
