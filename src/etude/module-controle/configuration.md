# Configuration
La configuration du module de contrôle contient plusieurs éléments:
* Les numéros de téléphones à notifier
* La quantité de médicaments dans chaque distributeur
* Les heures auxquelles distribuer chaque médicament
* L'empreinte digitale du patient

Pour faire cette configuration, l'application mobile va se connecter au module de contrôle par WiFi ou Bluetooth. L'important est que le module ne puisse pas être configuré à distance pour des causes de sécurité. C'est pour cela que si le WiFi est utilisé pour la configuration, le module de contrôle ne se connectera pas à un réseau, il fera son propre point d'accès pour ne pas qu'on puisse le connecter à l'Internet.
