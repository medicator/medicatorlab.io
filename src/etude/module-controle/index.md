# Module de contrôle

## Diagramme schématique
> ![Image Manquante](../../images/schematique-module-controle.png)  
Diagramme schématique

#### Tableau des composants
![Image Manquante](../../images/tableau-comps-controle.png)

#### Tableau des connecteurs
![Image Manquante](../../images/tableau-conns-controle.png)

#### Tableau des alimentations
![Image Manquante](../../images/tableau-alims-controle.png)

#### Tableau des derniers codes de référence
![Image Manquante](../../images/tableau-refsmax-controle.png)

## Image 3D
<!--
> ![Image 3D](../../images/3d-module-controle.png)
-->

> ![Image Manquante](https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png)

## Calculs

### Résistance de la LED rouge
\\( V_{cc} = 5V, V_f = 2V, I_f = 20mA \\)  
\\( R = \frac {V_{cc} - V_f}{I_f} \\)  
\\( R = \frac {5 - 2}{0.02} \\)  
\\( R = \frac {3}{0.02} \\)  
\\( R = 150 \Omega \\)  

### Résistance de la LED verte et de la bleue
\\( V_{cc} = 5V, V_f = 3.2V, I_f = 20mA \\)  
\\( R = \frac {V_{cc} - V_f}{I_f} \\)  
\\( R = \frac {5 - 3.2}{0.02} \\)  
\\( R = \frac {1.8}{0.02} \\)  
\\( R = 90 \Omega \\)  
