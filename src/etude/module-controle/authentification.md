# Authentification
Il est nécessaire de trouver un système pour authentifier le patient qui soit peut intrusif et rapide.

## Caractéristiques recherchées
* Prix maximal: 50$
* Complexité d'utilisation: faible
* Temps d'authentification maximal: 5 secondes
* Taux de rejection erronée maximal: 10%
* Taux d'acceptation erronée maximal: 0.1%, on ne veut pas que quelqu'un de la même famille, par exemple, puisse accéder au dispositif sans y être autorisé.
* Permet d'authentifier au moins une personne
* Pas trop intrusif

## Méthodes d'authentification biométriques
* Empreinte digitale: abordable et facile à utiliser
* Visage (image): trop cher et trop compliqué à utiliser
* Visage (3D): trop cher et trop compliqué à utiliser
* Iris: trop cher et trop compliqué à utiliser

Mis appart les capteurs d'empreintes digitales, tous les autres coûtent trop chers et sont trop difficile à utiliser. Beaucoup nécessiterait de connecter une caméra à un microcontrôleur puissant avec un système d'exploitation comme le Raspberry Pi.

## Capteurs d'empreintes digitales envisagés

#### [GT511C3 avec Arduino](https://circuitdigest.com/microcontroller-projects/arduino-fingerprint-sensor-gt511c3-interfacing)
* Désavantages:
    * On ne peut pas l'acheter des distributeurs du Cégep.

#### [(SENS-120) FPM10A](https://abra-electronics.com/robotics-embedded-electronics/breakout-boards/sensors/sens-120fpm10a-fingerprint-reader-sensor-for-microcontrollers.html)
* Prix: 19.95$
* Feuille du manufacturier: <https://www.robotshop.com/media/files/pdf/user-manual-sd-fpm10a_b7.pdf>
* Source: <https://artofcircuits.com/product/fpm10a>
* Alimentation:
  * Tension: 3 à 5V
  * Courant:
    * Typique: < 120mA
    * Maximal: 150mA
* Temps de reconnaissance d'une empreinte: < 1s
* Taille de la fenêtre du lecteur: 14 x 18 mm
* Capacités de stockage d'empreintes: 300
* Taux d'acceptation erroné (FAR): < 0.001% (3<sup>e</sup> degré de sécurité)
* Taux de rejection erroné (FRR): < 1% (3<sup>e</sup> degré de sécurité)
* Temps de recherche: < 1s
* Interface de communication: UART (niveaux logiques TTL)
* Vitesse de communication (UART): (9600 x `N`) baud où `N` = 1~12 (la valeur par défaut de `N` est 6, donc 57600 baud)
* Environnement de fonctionnement:
  * Température: -20°C à +50°C
  * Humidité relative: 40% à 85%
* Dimensions (longueur x largeur x hauteur): 46.25 x 20.4 x 18.1mm
* Avantages: prix, qualité des documents, exemples sur Internet avec Arduino qu'on pourrait utiliser pour apprendr à l'utiliser et tester son fonctionnement.
* Désavantages:
  * Courant plus élevé.

#### [SEN-15338](https://www.digikey.ca/product-detail/en/sparkfun-electronics/SEN-15338/1568-SEN-15338-ND/10279703)
* Prix: 30.48$
* Feuille du manufacturier: <https://cdn.sparkfun.com/assets/e/8/a/8/2/AD-013_Data_Sheet_v1.2_2018-10-25.pdf>.
* Commandes: <https://cdn.sparkfun.com/assets/f/c/a/e/e/AD-013_Command_Operation_Guide_v1.2.pdf>.
* Manuel d'utilisateur: <https://cdn.sparkfun.com/assets/e/c/d/a/1/MCU_SOC_Manual_EN.pdf>.
* Alimentation:
    * Tension: 3 à 3.6V
    * Courant:
      * Actif: 40mA
      * Veille: 18mA
* Interface de communication
* Taille de la fenêtre du lecteur: 8mm x 8mm
* Dimensions (longueur x largeur x hauteur): 29 x 19.6 x 6.06mm
* Interface de communication: UART
* Temps d'authentification: 0.6s
* Environnement de fonctionnement:
  * Température: -20°C à +70°C
  * Humidité relative: 0% à 95%
* Avantages:
  * Il est plat et beaucoup plus petit.
  * Il consomme moins d'énergie.
  * Il reconnaît plus rapidement les empruntes.
* Désavantages:
  * Selon les personnes l'ayant acheté, il est dur à utiliser et a peu de support.
  * Les documents qui y sont associés sont bourrés d'erreurs.
  * Il faudrait avoir un régulateur de tension supplémentaire pour ce dernier.
  * Il coûte un peu plus cher.

## Conclusion
Bien qu'il consomme plus de courant et soit un peu plus gros, j'ai choisi d'utiliser le FPM10A pour ses exemples déjà disponibles et sa simplicité d'utilisation.

Si mes modules utilisait une batterie j'aurais assurément sélectionné le SEN-15338 et me serais débrouillé pour le faire fonctionner, car il consomme tellement moins de courant en plus d'être plus petit et rapide.
