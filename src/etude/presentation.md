# Présentation du rapport
[mdBook] est l'outil que j'utilise pour générer un site pour mon rapport. Il prend un dossier contenant des images et des fichiers écrits en Markdown et en fait un beau site en HTML avec des fonctionnalités intéressantes comme d'avoir une barre de recherche et un bouton pour exporter tout le site dans un seul document PDF. Le markdown nous donne également plein d'avantages comme de pouvoir avoir du code bien formaté et coloré.

## Problèmes de [mdBook]
Malheureusement, [mdBook] n'a pas que des avantages.

### Exporter un PDF
Cependant, lorsque le site est exporté en PDF, il perd beaucoup de ses avantages. En effet, un des avantages d'un site est de pouvoir y placer toutes sortes de ressources autres que des fichiers HTML comme des fichiers PDF, chose que je fais pour mes feuilles de manufacturiers dans la section [Annexe](../annexe/index.md). Cependant, lorsque le site est converti en PDF, toutes les pages contenant des liens vers ces ressources ne sont tout simplement pas incluses dans le PDF. Je ne peux donc pas mettre de liens vers des fichiers compressés contenant du code ou d'autres choses.

### Annotation du rapport
Comme le rapport n'est pas un PDF qui peut facilement être imprimé ou un document [LibreOffice Writer] ou [Microsoft Word] qui peuvent facilement être annoté, il risque d'être plus dur pour mon superviseur d'annoter mon rapport.

### Affichage de code
Dans le dernier rapport utilisant cet outil que j'ai remis, j'ai tout simplement remis le code et les livrables dans des fichiers compressés. Ce n'est pas très convivial puisque le fichier doit être téléchargé, décompressé puis ouvert avec un éditeur de texte. Je pense pouvoir faire mieux que ça.

### Correction du français
Contrairement à [LibreOffice Writer] et à [Microsoft Word], mon éditeur de texte ([Atom](https://atom.io/) ou [VSCode](https://code.visualstudio.com/)) n'a pas de correcteur de français intégré.

## Améliorations
Heureusement, il y a des solutions à certains de ces problèmes.

### Mettre mon rapport sur mon site
En plaçant mon rapport sur un site Internet, mon responsable pourra plus facilement y avoir accès et n'aura pas à télécharger les modifications que j'y fait à chaque fois. Je pourrais facilement le faire, puisque je possède déjà un domaine relié à GitLab Pages et que je peux configurer GitLab CI.

### Extensions d'annotation
Plusieurs extensions pour navigateurs permettent à des utilisateurs d'annoter des pages webs et de partager ces annotations. Ceci pourrait être très utile pour mon superviseur puisqu'il pourrait me donner des commentaires sur mon rapport au fil du projet ou à la fin lorsque je lui aurais remis. Ces options n'ont cependant pas l'air aussi intéressantes qu'un service que j'ai trouvé.

Ce service s'appelle [Hypothesis](https://web.hypothes.is/) et est une organisation à but non lucratif qui utilise la librairie open source [Annotator.js](http://annotatorjs.org/) pour ajouter des fonctionnalités semblables à [Medium] à un site Internet. Il permet aux utilisateurs de créer des notes publiques, privées ou partagées avec certaines personnes. Mon évaluateur pourrait donc avoir ses propres notes et en partager certaines avec moi. Ceci ne nécessite pas l'installation d'une extension, mais elle nécessite de l'ajouter au site, ce qui semble toutefois être très facile.

<!--Pour [Microsoft Edge](https://www.microsoft.com/en-us/windows/microsoft-edge), veuillez voir [cet article](https://support.microsoft.com/fr-ca/help/17221/windows-10-write-on-the-web) qui explique comment utiliser l'annotateur intégré.-->

### Vérifier les liens
Lorsque j'ai remis mon dernier rapport utilisant [mdBook], j'ai eu très peur que certains de mes liens soient brisés sans que je m'en rende compte. Cette fois-ci j'utiliserai une extension de [mdBook] qui s'appelle [linkcheck](https://crates.io/crates/mdbook-linkcheck) et qui permet de vérifier tous les liens du site pour s'assurer qu'ils sont valides.

### Correction du français
Pour corriger mes fautes de français, je peux utiliser [LanguageTool] qui est un [gratuiciel](https://fr.wikipedia.org/wiki/Freeware) permettant de corriger les fautes de français en surlignant le texte à corriger dans une page HTML. Il offrent une extension pour navigateur simple à utiliser. Le problème avec ce service est qu'il ne corrige pas toutes les fautes pour nous inciter à utiliser la version payante. Je pourrais donc payer pour cette version à la place.

Je pourrais aussi utiliser [Grammalecte] qui est l'extension open source que j'utilise dans [LibreOffice Writer] pour corriger mes textes. Elle est disponible en tant qu'extension pour les navigateurs et certains éditeurs de texte comme [Sublime Text](https://www.sublimetext.com/). Le problème avec [Grammalecte] est que leur philosophie est d'éviter à tout prix de marquer du texte qui n'est pas erroné, donc ils trouvent beaucoup moins d'erreurs que [LanguageTool] qui va souvent se tromper. Je peux cependant facilement voir que [LanguageTool] s'est trompé et ignorer la correction.

En plus d'un correcteur, je peux configurer mes éditeurs de texte pour utiliser un dictionnaire français afin qu'ils puissent corriger mes erreurs d'orthographe.

### Exporter le site en [EPUB]
Le format [EPUB] est beaucoup utilisé pour les livres et est supporté par beaucoup de logiciels et [mdBook] permet d'exporter le site en EPUB, mais c'est une fonctionnalité est [expérimentale](https://crates.io/crates/mdbook-epub). Je ne sais pas non plus si j'aurais tous les avantages que j'avais avec une page web en utilisant ce format, même si un EPUB n'est qu'un dossier compressé contenant des fichiers HTML. Je pourrais donc le tester, mais je doute que je vais l'utiliser.

### Visualiseur de code
Bien qu'il soit très compliqué de bien afficher du code dans un document [Microsoft Word] ou [LibreOffice Writer], il est très simple de le faire avec [mdBook]. Cependant, il n'est pas simple d'afficher un dossier de projet et j'ai un dossier de projet pour chacun de mes tests et j'en aurai pour mon application mobile, mes modules de distribution et mon module de contrôle.

Je pense qu'il serait possible pour moi de créer ma propre extension pour [mdBook] qui permet de rendre un fichier `README.md` plus interactif. Je pense que cette fonctionnalité ne fonctionnerait que si mon rapport est une page web, mais elle permettrait de facilement voir l'arborescence d'un projet et le contenu de chacun de ses fichiers et que le code y soit bien formaté et coloré.

Pour les projets où seulement une partie du code est importante, comme dans mes tests où seul le fichier de configuration de PlatformIO et le fichier main.cpp sont importantes, je pourrais tout simplement importer le contenu de ces fichiers dans un bloc de code dans le `README.md` à l'aide d'une [syntaxe spéciale de mdBook](http://rust-lang.github.io/mdBook/format/mdbook.html?highlight=include#including-files).

### Affichage des images
Je pourrais très facilement faire en sorte que toutes les images soient également un lien vers elle-même afin qu'elles s'ouvrent dans un onglet de navigateur lorsqu'on clique dessus. On pourrait ainsi mieux voir les images importantes comme les schématiques.

Il serait aussi possible de modifier la page HTML de manière à ce qu'on puisse agrandir une image sans même avoir besoin de changer de page tout comme on peu le faire dans [Medium].

<!--
:active
-->

## Conclusion
Je ne sais pas qui sera mon superviseur, mais j'aimerai vraiment qu'il me permette d'utiliser [mdBook] et je ferais de mon mieux pour que le format de mon rapport ne vous nuise pas et que, au contraire, il vous avantage.

[mdBook]: https://rust-lang.github.io/mdBook/
[LibreOffice Writer]: https://www.libreoffice.org/discover/writer/
[Microsoft Word]: https://products.office.com/en-ca/word?rtc=1
[Grammalecte]: https://grammalecte.net/
[LanguageTool]: https://languagetool.org/
[Medium]: https://medium.com/
[EPUB]: https://en.wikipedia.org/wiki/EPUB
