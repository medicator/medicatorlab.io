# Architecture générale
> ![Image Manquante](../images/diag-fonc.png)  
Diagramme fonctionnel

Comme mentionné dans le cahier des charges, ce système sera séparé en trois parties: le module de contrôle, les modules de distribution et un téléphone mobile.

Le module de contrôle est celui qui sera connecté au réseau GSM. Il ne contiendra aucun médicament et sera relié à tous les modules de distribution avec un bus I²C. C'est lui qui va authentifier le patient et envoyer un signal par le bus I²C aux modules de distribution de distribuer des médicaments. Il sera aussi responsable d'envoyer des notifications au patient pour qu'il prenne ses médicaments et aux soignants lorsque le patient ne l'a pas fait.

Les modules de distribution sont ceux qui contiendront les médicaments. Ils recevront des signaux du module de contrôle pour savoir qu'ils doivent distribuer un médicament. En plus de faire le décompte du nombre de médicaments dans le distributeur au fur et à mesure qu'ils sont distribués, ils auront aussi un détecteur de bas niveau de médicament au cas où le nombre de médicaments chargés dans le distributeur est inférieur à celui configuré. Lorsque c'est le cas, une notification sera envoyée au module de contrôle pour qu'il puisse notifier le patient, le soignant et le pharmacien.

L'application va permettre au patient, au soignant et au pharmacien de recevoir les notifications envoyées par textos par le module de contrôle et d'obtenir des informations sur le nombre de médicaments restants dans le système et de le configurer.
