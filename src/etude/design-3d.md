# Design 3D

Je veux que ce soit un logiciel qui fonctionne sur Linux et qui, idéalement, soit open source.

## OpenSCAD ou OpenJSCAD
* Open Source
* Cross-platform

Permet de déclarer des variables et de créer des relations mathématiques pour calculer toutes les dimensions.

L'avantage de OpenSCAD par-dessus OpenJSCAD est que le language d'OpenSCAD a été fait spécifiquement pour cette application. Cependant, son language a moins de fonctionnalités que le JavaScript et tout ce qui peut être fait dans le language de OpenSCAD peut aussi être fait en JavaScript. Malheureusement, OpenJSCAD est encore en version alpha.

## FreeCAD
* Open Source
* Cross-platform

Il a l'avantage de permettre de positionner les éléments les uns par rapport aux autres, mais ne permet pas de déclarer des relations mathématiques aussi complexes que OpenSCAD. Il est toutefois beaucoup plus facile à utiliser.

Il est beaucoup plus lent que OpenSCAD.

Même s'il promet de pouvoir aller modifier toutes les opérations effectuées pour arriver au résultat, certaines modifications brisent celles qui suivent.

Le concept est fantastique, mais l'implémentation laisser à désirer. J'ai perdu beaucoup d'heures à corriger les erreurs qui survenaient lorsque je modifiait une pièce que j'avais créée.

Si c'était à refaire, j'utiliserai OpenSCAD, mais j'ai l'impression d'avoir passé trop de temps dans FreeCAD pour que ça vaille la peine de faire le changement.

## AutoCAD Fusion
* Closed Source
* Gratuit, grâce à mon statut d'étudiant
* Ne fonctionne que sur Windows
