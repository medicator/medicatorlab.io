[Chapitre 9: SMS](https://www.u-blox.com/en/docs/UBX-17003787#%5B%7B%22num%22%3A1265%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C59.527%2C742.677%2Cnull%5D)

Model: SARA-R410M-02B

## 9.1.2 <index> parameter range
The <index> parameter range depends on the memory storage type:
ME (ME message), SM ((U)SIM message) MT (ME + SM):
*  Values between 0 and 23: SMS stored in ME.
*  Values between 0 and n: SMS stored in SIM (n depends on SIM card used).

## Single SMS
160 caractères à 7 bits, 140 à 8 bits ou 70 à 16 bit

Acknolewdge incomming messages: `AT+CSMS=1`  
Text mode: `AT+CMGF=1`  
Send message: `AT+CMGS="0171112233"<CR>Allo maman`  
printf 'AT+CMGS="8199685918"\rSil te plaît dit moi que ça fonctionne\32\r\n' >> /dev/ttyACM0
