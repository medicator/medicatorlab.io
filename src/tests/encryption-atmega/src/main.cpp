/**
 * @brief Test la communication I²C en tant que maître et en tant qu'esclave.
 *
 * Par défaut le contrôleur est en mode esclave et est prêt à accepter des
 * messages. Il ne se met en mode maître que lorsque nécessaire.
 *
 * Lorsqu'il est en mode esclave (à l'adresse 9), il affiche les données qu'il
 * reçoit sur le port sériel et répond aux requêtes de données avec un message
 * qui peut être configuré.
 *
 * Lorsqu'il reçoit 'ecrire <msg>' sur son port sériel, le contrôleur se
 * configure en tant que maître et envoi le message donné à l'esclave à
 * l'adresse ADRESSE_PAIR.
 *
 * Lorsqu'il reçoit 'lire <msg>', le contrôleur se configure en tant que maître
 * et envoi le message donné à l'esclave à l'adresse ADRESSE_PAIR puis lit sa
 * réponse et l'affiche.
 *
 * Lorsqu'il reçoit un message formatté de n'importe quelle autre façon, le
 * message donné est utilisé pour répondre aux prochaines requêtes qui lui sont
 * envoyées lorsqu'il est en mode esclave.
 */

#include <AES.h>
#include <Arduino.h>
#include <CBC.h>
#include <CRC32.h>
#include <Crypto.h>
#include <CryptoLegacy.h>
#include <Wire.h>

//! Taille d'un bloc
#define AES_BLOCKLEN 16
//! Taille de la clé d'encryption
#define AES_KEYLEN (256 / 8)
//! Taille du vecteur d'initialisation AES
#define TAILLE_IV 16
//! Taille des données encryptées pour la structure TMsg
#define TAILLE_DONNEES (10 * AES_BLOCKLEN)
//! Taille d'un paquet transféré sur le port I²C
#define TAILLE_SEGMENT 32

//! Message transféré sur le port I²C
struct TMsg {
    //! Clé d'encryption AES 256
    uint8_t cle[AES_KEYLEN];
    //! Vecteur d'initialisation
    uint8_t iv[TAILLE_IV];
    //! Données encryptées
    uint8_t donnees[TAILLE_DONNEES];
    //! Code CRC (little endian) qui s'applique à toutes les données de la
    //! structure avant qu'elles soient encryptées, sauf à lui-même
    uint32_t crc;
};

//! Message à envoyer.
TMsg msg;

//! Message décryté transféré.
static char message[TAILLE_DONNEES];

//! Adresse de ce contrôleur en mode esclave.
static const int ADRESSE = 8;
//! Adresse de l'esclave.
static const int ADRESSE_PAIR = 9;

void onReceive(int octets);
void onRequest();
void preparerMsg();

void setup() {
    Serial.begin(9600);

    Wire.begin(ADRESSE);
    Wire.onReceive(onReceive);
    Wire.onRequest(onRequest);

    Serial.println("Initialisation terminée");
}

void erreurEndTransmission(byte b) {
    const char *message;
    switch (b) {
    case 0:
        message = "Aucune erreur de transmission survenue";
        break;
    case 1:
        message = "Trop de données pour le buffer I²C";
        break;
    case 2:
        message = "NACK reçu lors de la transmission de l'adresse";
        break;
    case 3:
        message = "NACK reçu lors de la transmission des données";
        break;
    default:
        message = "Erreur inconnue survenue";
    }
    Serial.println(message);
}

void loop() {
    // Lire une ligne entrée par l'utilisateur.
    char c;
    int i = 0;
    do {
        // Attendre le prochain caractère.
        while (Serial.available() == 0) {
        }

        c = Serial.read();
        Serial.print(c);
        message[i] = c;
        i++;
    } while (c != '\n');
    message[i - 2] = 0;

    Serial.print("Envoi du message: ");
    Serial.println(message);

    // Préparer le message à envoyer.
    preparerMsg();

    // Envoyer le message.
    for (size_t i = 0; i < sizeof(TMsg); i += TAILLE_SEGMENT) {
        int taille = sizeof(TMsg) - i;
        if (taille > TAILLE_SEGMENT) {
            taille = TAILLE_SEGMENT;
        }

        Wire.beginTransmission(ADRESSE_PAIR);
        Wire.write(&((uint8_t *)&msg)[i], taille);
        erreurEndTransmission(Wire.endTransmission());
    }

    // Lire la réponse.
    Wire.requestFrom(ADRESSE_PAIR, 1);
    Serial.print("Code d'erreur reçu: ");
    Serial.println((char)Wire.read());
}

volatile bool transfertPrecedentReussi = false;
uint8_t messageEncrypte[TAILLE_DONNEES];

//! Index dans le tampon de réception.
volatile size_t iRx = 0;
//! Tampon pour recevoir le message.
volatile TMsg rx;

/**
 * @brief Reçoit un message du port I²C et l'envoi sur le port sériel.
 */
void onReceive(
    //! Nombre d'octets reçus
    int octets) {
    // Placer toutes les données reçues dans le tampon.
    while (Wire.available() > 0 && iRx < sizeof(TMsg)) {
        ((uint8_t *)&msg)[iRx] = Wire.read();
        iRx++;
    }
    // Ignorer le reste des données reçues.
    while (Wire.available() > 0) {
        Wire.read();
    }

    if (iRx >= sizeof(TMsg)) {
        iRx = 0;

        Serial.println();
        Serial.println();
        for (int i = 0; i < sizeof(TMsg); i++) {
            uint8_t c = ((uint8_t *)&msg)[i];
            int j = (int)c;
            Serial.printf("0x%.2x,", j);
        }
        Serial.println();
        Serial.println();

        // Copier le message dans un deuxième tampon.
        memcpy(messageEncrypte, msg.donnees, TAILLE_DONNEES);

        // Décrypter les données.
        CBC<AES256> ctx;
        ctx.setKey(msg.cle, AES_KEYLEN);
        ctx.setIV(msg.iv, TAILLE_IV);
        ctx.decrypt(msg.donnees, messageEncrypte, TAILLE_DONNEES);

        // Calculer le CRC.
        uint32_t crc =
            CRC32::calculate((uint8_t *)&msg, sizeof(TMsg) - sizeof(uint32_t));

        transfertPrecedentReussi = crc == msg.crc;

        // Afficher le message reçu.
        msg.donnees[TAILLE_DONNEES - 1] = 0;
        Serial.print("Message reçu: ");
        Serial.println((char *)msg.donnees);
    }
}

/**
 * @brief Envoi la variable `reponse` sur le port sériel.
 */
void onRequest() {
    Wire.print((char)transfertPrecedentReussi);
    transfertPrecedentReussi = false;
}

/**
 * @brief Prépare la structure `msg` en calculant son CRC et en encryptant ses
 * données.
 */
void preparerMsg() {
    // Générer une clé aléatoire.
    for (int i = 0; i < AES_KEYLEN; i++) {
        msg.cle[i] = (uint8_t)rand();
    }

    // Générer un IV aléatoire.
    for (int i = 0; i < TAILLE_IV; i++) {
        msg.iv[i] = (uint8_t)rand();
    }

    // Copier le message dans la structure de message (ceci est nécessaire pour
    // le calcul du CRC).
    memcpy(msg.donnees, message, TAILLE_DONNEES);

    // Calculer le CRC (le byte order du ATMEGA328P est little endian).
    msg.crc =
        CRC32::calculate((uint8_t *)&msg, sizeof(TMsg) - sizeof(uint32_t));

    // Encrypter les données.
    CBC<AES256> ctx;
    ctx.setKey(msg.cle, AES_KEYLEN);
    ctx.setIV(msg.iv, TAILLE_IV);
    ctx.encrypt(msg.donnees, (uint8_t *)message, TAILLE_DONNEES);
}
