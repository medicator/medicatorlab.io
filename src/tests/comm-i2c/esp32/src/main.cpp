/**
 * @brief Test la communication I²C en tant que maître et en tant qu'esclave.
 *
 * Par défaut le contrôleur est en mode esclave et est prêt à accepter des
 * messages. Il ne se met en mode maître que lorsque nécessaire.
 *
 * Lorsqu'il est en mode esclave (à l'adresse 9), il affiche les données qu'il
 * reçoit sur le port sériel et répond aux requêtes de données avec un message
 * qui peut être configuré.
 *
 * Lorsqu'il reçoit 'ecrire <msg>' sur son port sériel, le contrôleur se
 * configure en tant que maître et envoi le message donné à l'esclave à
 * l'adresse ADRESSE_PAIR.
 *
 * Lorsqu'il reçoit 'lire <msg>', le contrôleur se configure en tant que maître
 * et envoi le message donné à l'esclave à l'adresse ADRESSE_PAIR puis lit sa
 * réponse et l'affiche.
 *
 * Lorsqu'il reçoit un message formatté de n'importe quelle autre façon, le
 * message donné est utilisé pour répondre aux prochaines requêtes qui lui sont
 * envoyées lorsqu'il est en mode esclave.
 */

#include <Arduino.h>
#include <Wire.h>

//! Adresse de ce contrôleur en mode esclave.
static const int ADRESSE = 9;
//! Adresse de l'esclave.
static const int ADRESSE_PAIR = 8;

void erreurEndTransmission(byte b) {
    const char *message;
    switch (b) {
    case 0:
        message = "Aucune erreur de transmission survenue";
        break;
    case 1:
        message = "Trop de données pour le buffer I²C";
        break;
    case 2:
        message = "NACK reçu lors de la transmission de l'adresse";
        break;
    case 3:
        message = "NACK reçu lors de la transmission des données";
        break;
    default:
        message = "Erreur inconnue survenue";
    }
    Serial.println(message);
}

void setup() {
    Serial.begin(115200);

    int e = Wire.begin();
    Serial.print("Erreur begin: ");
    Serial.print(e);
    Serial.print(", ");
    Serial.println(Wire.lastError());
    delay(1000);
    Serial.print("Envoi du message: ");
    erreurEndTransmission((byte)Wire.writeTransmission((uint16_t)ADRESSE_PAIR, (uint8_t *)"allo", 4));

}

void loop() {
}
