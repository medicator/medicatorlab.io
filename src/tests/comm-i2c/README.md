# Communications I²C entre le ESP32 et le ATMEGA328P

## But

## Hypothèse

## Démarche

## Résultats

## Analyse
Ne pas oublier de connecter les grounds ensemble.
Vérifier que les niveaux logiques sont compatibles (0-5V et 0-3.3V).

## Problèmes
#### [Le mode slave pour la librairie Arduino du ESP32 n'est pas implémenté](https://github.com/espressif/arduino-esp32/issues/118)
`Wire::onRequest` et `Wire::onReceive`

## Conclusion
