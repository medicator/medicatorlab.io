# Informations pour implémenter le mode slave/master I²C

Document: https://www.espressif.com/sites/default/files/documentation/esp32_technical_reference_manual_en.pdf

## Chapitres importants
* 1.3: Address spaces
* 1.3.5: Peripheral addresses
    * I2C0: bus=Data, low=0x3FF5_30000, high=x3FF5_3FFF, size=4KB

* 2.3.1: Interrupt peripheriques
* 11: I²C controller
