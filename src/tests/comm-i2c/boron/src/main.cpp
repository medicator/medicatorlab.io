/**
 * @brief Test la communication I²C en tant que maître et en tant qu'esclave.
 *
 * Par défaut le contrôleur est en mode esclave et est prêt à accepter des
 * messages. Il ne se met en mode maître que lorsque nécessaire.
 *
 * Lorsqu'il est en mode esclave (à l'adresse 9), il affiche les données qu'il
 * reçoit sur le port sériel et répond aux requêtes de données avec un message
 * qui peut être configuré.
 *
 * Lorsqu'il reçoit 'ecrire <msg>' sur son port sériel, le contrôleur se
 * configure en tant que maître et envoi le message donné à l'esclave à
 * l'adresse ADRESSE_PAIR.
 *
 * Lorsqu'il reçoit 'lire <msg>', le contrôleur se configure en tant que maître
 * et envoi le message donné à l'esclave à l'adresse ADRESSE_PAIR puis lit sa
 * réponse et l'affiche.
 *
 * Lorsqu'il reçoit un message formatté de n'importe quelle autre façon, le
 * message donné est utilisé pour répondre aux prochaines requêtes qui lui sont
 * envoyées lorsqu'il est en mode esclave.
 */
#include <Arduino.h>
#include <Wire.h>

//! Taille d'un paquet envoyé sur le port I²C lorsque ce contrôleur agit en tant
//! que maître.
#define TAILLE_SEGMENT 32

//! Réponse à une requête du maître I²C.
static String reponse = "Message non initialisé";

//! Adresse I²C de ce MCU en tant qu'esclave.
const int ADRESSE = 9;
//! Adresse I²C de l'esclave.
const int ADRESSE_PAIR = 8;

void onReceive(int);
void onRequest();

void setup() {
  Serial.begin(9600);

  // Initialiser le port I²C en tant qu'esclave.
  Wire.begin(ADRESSE);
  Wire.onReceive(onReceive);
  Wire.onRequest(onRequest);

  Serial.println("Initialisation terminée");
}

/**
 * @brief Affiche le message d'erreur associé au coude donné.
 */
void erreurTransmission(uint8_t codeErreur) {
  const char *message;
  switch (codeErreur) {
  case 0:
    message = "Aucune erreur de transmission survenue";
    break;
  case 1:
    message = "Trop de données pour le buffer I²C";
    break;
  case 2:
    message = "NACK reçu lors de la transmission de l'adresse";
    break;
  case 3:
    message = "NACK reçu lors de la transmission des données";
    break;
  default:
    message = "Erreur inconnue survenue";
  }
  Serial.println(message);
}

void loop() {
  String commande;

  // Lire une ligne entrée par l'utilisateur.
  char c;
  do {
    // Attendre le prochain caractère.
    while (Serial.available() == 0) {
    }

    c = Serial.read();
    Serial.print(c);
    commande += c;
  } while (c != '\n');
  commande = commande.substring(0, commande.length() - 2);

  // Si la demande est d'envoyer un message sur le port
  if (commande.startsWith("ecrire ")) {
    // Configurer le contrôleur I²C en mode maître.
    Wire.begin();

    commande = commande.substring(7);

    Serial.print("Envoi du message: ");
    Serial.println(commande);

    for (uint8_t i = 0; i < commande.length(); i += TAILLE_SEGMENT) {
      Wire.beginTransmission(ADRESSE_PAIR);
      int tailleMsg = min(commande.length() - i, TAILLE_SEGMENT);
      Wire.write((uint8_t*)&commande[i], tailleMsg);
      erreurTransmission(Wire.endTransmission(tailleMsg < TAILLE_SEGMENT));
    }

    // Configurer le contrôleur I²C en mode esclave.
    Wire.begin(ADRESSE);
  } else if (commande.startsWith("lire ")) {
    // Configurer le contrôleur I²C en mode maître.
    Wire.begin();

    if (commande.length() > 5) {
      commande = commande.substring(5);

      Serial.print("Envoi du message: ");
      Serial.println(commande);

      // Envoyer le message.
      Wire.beginTransmission(ADRESSE_PAIR);
      Wire.print(commande);
      erreurTransmission(Wire.endTransmission());
    }
    // Lire la réponse.
    Wire.requestFrom(ADRESSE_PAIR, TAILLE_SEGMENT);
    Serial.print("Réception du message: ");
    Serial.println(Wire.readString());

    // Configurer le contrôleur I²C en mode esclave.
    Wire.begin(ADRESSE);
  } else {
    reponse = commande;
  }
}

/**
 * @brief Reçoit un message du port I²C et l'envoi sur le port sériel.
 */
void onReceive(
    //! Nombre de bytes reçus
    int octets) {
  (void)octets;

  // Message reçu.
  static char message[TAILLE_SEGMENT + 1];

  // Recevoir le message.
  int i = 0;
  for (i = 0; i < TAILLE_SEGMENT && Wire.available() > 0; i++) {
    message[i] = (char)Wire.read();
  }
  message[i] = 0;

  // Ignorer les caractères restants (il ne devrait pas y en avoir de toutes les
  // façons).
  while (Wire.available() > 0) {
    Wire.read();
    i++;
  }

  // Afficher le message.
  Serial.print("Message reçu: ");
  Serial.println(message);
}

/**
 * @brief Envoi la variable `reponse` sur le port sériel.
 */
void onRequest() {
  Wire.write(reponse.c_str());
  Serial.println("Message envoyé");
}
