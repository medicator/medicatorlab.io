# Communication I²C

## But
Communiquer entre le Boron et le ATMEGA328P.

## Hypothèse
Je pense pouvoir tout simplement copier le code source du ATMEGA328P et je pense que ça devrait fonctionner. Il est possible que j'ai besoin de jouer un peu avec le main en appelant `Wire.begin` avec et sans adresse pour le configurer en mode maître et esclave en fonction de mes besoins.

[API Wire de DeviceOS pour le Boron](https://docs.particle.io/reference/device-os/firmware/boron/#wire-i2c-)

## Démarche

## Résultats

## Analyse

## Problèmes

### Le firmware crash à chaque fois qu'il reçoit un message par I²C
Le ATMEGA328P envoit le message et n'a pas d'erreur, mais après la LED du boron clignotte en rouge ce qui signifie qu'il a crashé.
J'ai assumé que c'est parce que je passait trop longtemps dans ma fonction `onReceive`, ce qui était le cas. Je ne peux donc pas commencer à écrire sur le port sériel ou initialiser un string et y placer des données.

Ceci est probablement une limitation sur la durée des interruptions du RTOS ou sur les fonctions qui peuvent être utilisées dans un ISR sur cette plateforme.

J'ai testé ma théorique en ajoutant ceci dans la fonction `onReceive`: `free(malloc(1));`. En effet, si j'appelle cette fonction, le programme plante.

J'ai testé ma seconde théorie en enlevant la ligne précédemment ajoutée et en ceci dans la fonction `onReceive`: `Serial.println("Message reçu");`. Contrairement à ce que je pensais, appeler cette fonction ne fait pas planter le programme.

### Une fois que le Boron a envoyé un message en tant que maître, il ne reçoit plus de message en tant qu'esclave
Comme je l'avais prévu, il ne passe pas automatiquement de maître à esclave, ni d'esclave à maître. Il faut donc que je le fasse rebasculer en mode esclave en reconfigurant son adresse après l'avoir utiliser en tant que maître et que je le bascule en mode maître en enlevant son adresse.

## Conclusion
Le buffer RX de ce contrôleur semble également être de 32 octets (**à vérifier dans la documentation du MCU**).
