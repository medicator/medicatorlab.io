#!/bin/sh

PORT=$1
FICHIER=$2

# reset
printf '\03\03' >> $PORT
sleep 0.5
printf 'import machine; machine.reset()\r\n' >> $PORT
sleep 1


printf '\05' >> $PORT # paste mode
sleep 0.5
unix2dos < $FICHIER > $PORT # file
printf '\04\r\n' >> $PORT # end paste mode
