import machine

def i2c_cb(res):
    cbtype = res[0] # i2c slave cllback type
    if cbtype == machine.I2C.CBTYPE_TXDATA:
        print("[I2C] Data sent to master: addr={}, len={}, ovf={}, data={}".format(res[1], res[2], res[3], res[4]))
    elif cbtype == machine.I2C.CBTYPE_RXDATA:
        print("[I2C] Data received from master: addr={}, len={}, ovf={}, data: [{}]".format(res[1], res[2], res[3], res[4]))
        s.setdata("1234567890abcdefghij", 0)
    elif cbtype == machine.I2C.CBTYPE_ADDR:
        print("[I2C] Addres set: addr={}".format(res[1]))
        s.setdata("1234567890abcdefghij", 0)
    else:
        print("Unknown CB type, received: {}".format(res))

# Create two i2c instance objects, master and slave
#m = machine.I2C(1, sda=21, scl=22, speed=400000)
# no buffer length is specified, default 256 bytes buffer will be used
# with 8-bit addressing
s = machine.I2C(0, mode=machine.I2C.SLAVE, sda=21, scl=22, slave_addr=20)

# Enable all slave callbacks
s.callback(i2c_cb, s.CBTYPE_ADDR | s.CBTYPE_RXDATA | s.CBTYPE_TXDATA)

#Set some data in slave buffer
s.setdata("1234567890abcdefghij", 0)
s.setdata("ABCDEFGHabcdefgh", 0x80)
s.setdata("BUFFEREND", 0xF7)
