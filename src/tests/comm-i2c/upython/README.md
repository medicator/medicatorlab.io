tutoriel: https://docs.micropython.org/en/latest/esp32/tutorial/intro.html

## Uploader le firmware
version du firmware: *GENERIC : [esp32-idf4-20191220-v1.12.bin](https://micropython.org/resources/firmware/esp32-idf4-20191220-v1.12.bin)*

Télécharger esptool: `sudo pacman -S esptool`

Uploader le firmwarm:
```sh
firmware=esp32-idf4-20191220-v1.12.bin

wget https://micropython.org/resources/firmware/$firmware

esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 $firmware
```

## Utiliser
https://docs.micropython.org/en/latest/library/machine.I2C.html#machine-i2c

https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/i2c

https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/i2c#i2ccallbackfunc-type

## REPL
```sh
picocom /dev/ttyUSB0 -b 115200
```

## Nouveau problème
Vanilla micropython doesn't have I²C slave support, but Loboris fork does: https://github.com/micropython/micropython/issues/3680

Loboris fork: https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo

## Signaux
envoyer: `printf '\01' >> /dev/ttyUSB0`

raw repl (?): 0x01
restart (Ctrl+B): 0x02
cancel (Ctrl+C): 0x03
soft reboot (Ctrl+D): 0x04
paste mode (Ctrl+E): 0x05

## Séquence d'upload
```sh
PORT=/dev/ttyUSB0
FICHIER=main.py

printf '\02' >> $PORT # restart
printf '\05' >> $PORT # paste mode
cat "$FICHIER" >> $PORT # file
printf '\04' >> $PORT # end paste mode
```
