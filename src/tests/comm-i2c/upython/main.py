import machine
from machine import I2C, Pin

sda = Pin(21)
scl = Pin(22)
addr = 20
addr_pair = 8

i2c = I2C(0, mode=I2C.SLAVE, sda=sda, scl=scl, freq=1000, slave_addr=addr)

def callback(res):
    cbtype = res[0] # i2c slave cllback type
    if cbtype == machine.I2C.CBTYPE_TXDATA:
        print("[I2C] Data sent to master: addr={}, len={}, ovf={}, data={}".format(res[1], res[2], res[3], res[4]))
    elif cbtype == machine.I2C.CBTYPE_RXDATA:
        print("[I2C] Data received from master: addr={}, len={}, ovf={}, data: [{}]".format(res[1], res[2], res[3], res[4]))
        i2c.setdata("waddup", 0)
    elif cbtype == machine.I2C.CBTYPE_ADDR:
        print("[I2C] Addres set: addr={}".format(res[1]))
    else:
        print("Unknown CB type, received: {}".format(res))

i2c.callback(callback, I2C.CBTYPE_RXDATA | I2C.CBTYPE_TXDATA | machine.I2C.CBTYPE_ADDR)
i2c.setdata("waddup", 0)

while True:
    commande = input()
    if commande.startswith("ecrire "):
        commande = commande[7:]
        i2c.init(0, mode=I2C.MASTER, sda=sda, scl=scl, freq=1000)

        if not i2c.writeto(addr_pair, commande.encode()):
            print("Erreur écriture")

        i2c.init(0, mode=I2C.SLAVE, sda=sda, scl=scl, freq=1000, slave_addr=addr)
        i2c.callback(callback, I2C.CBTYPE_RXDATA | I2C.CBTYPE_TXDATA)
    elif commande.startswith("lire "):
        commande = commande[5:]

        i2c.init(0, mode=I2C.MASTER, sda=sda, scl=scl, freq=1000)

        if (len(commande) > 0):
            print("écriture: ", commande)
            if not i2c.writeto(addr_pair, commande.encode()):
                print("erreur écriture")

        bytes = i2c.readfrom(addr_pair, 32)
        print(bytes)

        i2c.init(0, mode=I2C.SLAVE, sda=sda, scl=scl, freq=1000, slave_addr=addr)
        i2c.callback(callback, I2C.CBTYPE_RXDATA | I2C.CBTYPE_TXDATA)
    else:
        reponse = commande
