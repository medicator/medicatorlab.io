/**
 *
 */
#ifndef MYWIRE_H
#define MYWIRE_H

#include <stdint.h>

#define SLAVE_BUF_LEN 1024 // 1kiB

//! FIFO buffer
class buffer {
public:
    //! Data of the buffer
    uint8_t data[SLAVE_BUF_LEN];
    //! Size of the buffer
    uint16_t size = 0;
    //! Position of the cursor in the buffer
    uint16_t index = 0;
    //! Whether the buffer has overflowed
    bool overflow = false;
    //! Whether the buffer has underflowed
    bool underflow = false;

    /**
     * @brief Returns the next byte in the buffer or -1 if it has been completely read and advances the cursor
     */
    int next() {
        if (size >index) {
            uint8_t result = data[index];
            index++;
            return result;
        } else {
            reset();
            underflow = true;
            return -1;
        }
    }
    /**
     * @brief Returns the next byte in the buffer or -1 if it has been completely read
     */
    int peak() {
        if (size > index) {
            return data[index];
        } else {
            return -1;
        }
    }
    /**
     * @brief Returns the number of bytes available
     */
    int available() {
        return size - index;
    }
    /**
     * @brief Resets the buffer and it's error flags
     */
    void reset() {
        underflow = false;
        overflow = false;
        size = 0;
        index = 0;
    }

    /**
     * @brief Append the given buffer to this one
     *
     * @returns error code
     * @retval 0 no error
     * @retval 1 the given buffer is bigger than this one
     * @retval 2 the buffer has overflowed
     */
    int append(uint8_t* buf, uint8_t size) {
        if (size > SLAVE_BUF_LEN) {
            return -1;
        }

        int error = 0; // error code.
        int start = 0; // idx where to start the transfer in the buffer.

        // If there's enough space at the end of the buffer for the new data
        if ((SLAVE_BUF_LEN - this->size) > size) {
            start = size; // Start after the last used byte of the buffer.
            this->size += size; // Increase the size.
        }
        // There's enough space in the buffer for the new data, but the old one needs to be shifted
        else if ((SLAVE_BUF_LEN - (size - index)) == 0) {
            // Copy the data to the beginning of the buffer.
            int shift = index;
            for (int i = 0; i < index; i++) {
                data[i] = data[shift - i];
            }

            index -= shift; // move the cursor.
            this->size = shift + size; // Update the buffer's size.
        }
        // There's not enough place for the new data, the old one needs to be overwritten
        else {
            error = 2;
            overflow = true;
            index = 0; // Reset the cursor.
        }

        // Copy over the data
        for (int i = 0; i < size; i++) {
            data[start + i] = buf[i];
        }

        // Return the error code.
        return error;
    }
};

//! Slave on I²C bus 0
class Slave {
private:
    //! Input buffer
    buffer rx;
    //! Output buffer
    buffer tx;
    //! I2C slave Address
    int address;

public:
    Slave();

    int begin(int address);
    int onRequest(void (*fn)(void));
    int onReceive(void (*fn)(int));

    int write(uint8_t byte);
    int read() {
        return rx.next();
    }
    int available() {
        return rx.available();
    }

    ~Slave();
};

#endif /* MYWIRE_H */
