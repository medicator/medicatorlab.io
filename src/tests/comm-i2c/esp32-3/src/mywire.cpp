#include "mywire.h"
#include <Arduino.h>
#include <soc/dport_reg.h>
#include <soc/i2c_reg.h>
#include <soc/i2c_struct.h>

#define I2C_BUF_SIZE 32
#define PORT0 0

#define SET(reg, mask) *(volatile uint32_t *)reg |= mask
#define CLEAR(reg, mask) *(volatile uint32_t *)reg &= ~(mask)

#include <driver/i2c.h>

/**
 *
 */
Slave::Slave() {
    // Rien à faire.
}

/**
 * @brief Initialise le contrôleur I²C du port 0 en mode esclave
 */
int Slave::begin(int address) {
    // Mode slave.
    for (int i = 0; i < 32; i++) {
        // Serial.println(i);
        /*Serial.println(I2C_CTR_REG(PORT0), BIN);
        DPORT_CLEAR_PERI_REG_MASK(I2C_CTR_REG(PORT0), BIT(i));
        Serial.println(I2C_CTR_REG(PORT0), BIN);
        DPORT_SET_PERI_REG_MASK(I2C_CTR_REG(PORT0), BIT(i));
        Serial.println(I2C_CTR_REG(PORT0), BIN);
        Serial.println(I2C_CTR_REG(PORT0), HEX);*/
    }
    // Serial.println(I2C_CTR_REG(PORT0), BIN);
    // CLEAR(I2C_CTR_REG(PORT0), I2C_MS_MODE);
    // DPORT_CLEAR_PERI_REG_MASK(I2C_CTR_REG(PORT0), I2C_MS_MODE);
    // Serial.println(I2C_CTR_REG(PORT0), BIN);
    // SET(I2C_CTR_REG(PORT0), I2C_MS_MODE);
    // DPORT_SET_PERI_REG_MASK(I2C_CTR_REG(PORT0), I2C_MS_MODE);
    // Serial.println(I2C_CTR_REG(PORT0), BIN);

    Serial.println(I2C0.ctr.ms_mode);

    i2c_config_t config = {
        I2C_MODE_SLAVE,     /*!< I2C mode */
        GPIO_NUM_22,        /*!< GPIO number for I2C sda signal */
        GPIO_PULLUP_ENABLE, /*!< Internal GPIO pull mode for I2C sda signal*/
        GPIO_NUM_21,        /*!< GPIO number for I2C scl signal */
        GPIO_PULLUP_ENABLE, /*!< Internal GPIO pull mode for I2C scl signal*/
    };
    config.slave.addr_10bit_en = 0;
    config.slave.slave_addr = address;
    i2c_param_config(I2C_NUM_0, &config);

    Serial.println(I2C0.ctr.ms_mode);

    config.mode = I2C_MODE_MASTER;
    i2c_param_config(I2C_NUM_0, &config);

    Serial.println(I2C0.ctr.ms_mode);

    return 0;
}

/*Master::begin {
I2C_SCL_LOW_PERIOD_REG

}*/

int Slave::onRequest(void (*fn)(void)) {
    return 0;
}

int Slave::onReceive(void (*fn)(int)) {
    return 0;
}

int Slave::write(uint8_t byte) {
    return 0;
}

Slave::~Slave() {
}
