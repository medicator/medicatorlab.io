# Informations pour implémenter le mode slave/master I²C

Document: https://www.espressif.com/sites/default/files/documentation/esp32_technical_reference_manual_en.pdf

## Chapitres importants
* 1.3: Address spaces
* 1.3.5: Peripheral addresses
    * I2C0: bus=Data, low=0x3FF5_30000, high=x3FF5_3FFF, size=4KB
* 2.3.1: Interrupt peripheriques
* 11: I²C controller

# Problèmes
1. La librarie ESP32 n'implémente pas le protocole I²C en tant qu'esclave: https://github.com/espressif/arduino-esp32/issues/118
2. Le code pour le I²C du framework arduino du ESP32 est mal commenté et est mal compris, même par ceux qui le maintiennent
3. Je n'arrive pas à utiliser esp-idf en tant que master reader: mon code
4. La librairie esp-idf n'a pas d'exemple de comment utiliser un ISR pour intercepter les messages reçus en tant qu'esclave: https://esp32.com/viewtopic.php?f=2&t=3009 et https://github.com/espressif/esp-idf/tree/master/examples/peripherals/i2c

## Note
Après avoir envoyer le dernier byte, l'esclave envoi un NACK.

## Problème
le buffer est de 32 bytes
