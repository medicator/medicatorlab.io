/**
 * @brief
 */

#include <Arduino.h>
#include <driver/i2c.h>
#include <soc/i2c_struct.h>

void onReceive(void *);
void onRequest();

String reponse = "Message non initialisé";
intr_handle_t *handleIsr = NULL;

const int ADRESSE_I2C = 20;
const int ADRESSE_PAIR = 8;
#define TAILLE_TAMPON 22 // 4KB
#define PORT_I2C I2C_NUM_0

const char *ERREURS_INIT[] = {
    "Aucune",       "Driver delete", "free isr",
    "register isr", "config params", "driver install",
};

int initI2C(i2c_mode_t mode) {
  i2c_config_t config;

  int prev_mode = -1;

  static DRAM_ATTR i2c_dev_t *const PORT = &I2C0;

  if (prev_mode != -1) {
    if (i2c_driver_delete(PORT_I2C) != ESP_OK) {
      return 1;
    }

    if (prev_mode == I2C_MODE_SLAVE) {
      if (i2c_isr_free(*handleIsr) != ESP_OK) {
        return 2;
      }
    }
  }

  prev_mode = mode;

  // Configurer le mode.
  config.mode = mode;

  // Configurer les pins.
  config.sda_io_num = GPIO_NUM_21;
  config.sda_pullup_en = GPIO_PULLUP_ENABLE;
  config.scl_io_num = GPIO_NUM_22;
  config.scl_pullup_en = GPIO_PULLUP_ENABLE;

  if (mode == I2C_MODE_SLAVE) {
    config.slave.addr_10bit_en = false;
    config.slave.slave_addr = ADRESSE_I2C;
    if (i2c_isr_register(PORT_I2C, onReceive, NULL, 0, handleIsr) != ESP_OK) {
      return 3;
    }
  } else {
    config.master.clk_speed = 1000; // 1kHz
  }

  if (i2c_param_config(PORT_I2C, &config) != ESP_OK) {
    return 4;
  }
  if (i2c_driver_install(PORT_I2C, mode, TAILLE_TAMPON, TAILLE_TAMPON, 0) !=
      ESP_OK) {
    return 5;
  }

  return 0;
}

const char *ERR_WRITE[] = {
    "Aucune erreur",       "master start", "master write byte (address)",
    "master write (data)", "master stop",  "master cmd begin",
};
int writeMaster(const uint8_t *donnees, size_t taille) {
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  if (i2c_master_start(cmd) != ESP_OK) {
    return 1;
  }

  if (i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_WRITE,
                            true) != ESP_OK) {
    return 2;
  }
  if (i2c_master_write(cmd, (uint8_t *)donnees, taille, true) != ESP_OK) {
    return 3;
  }
  if (i2c_master_stop(cmd) != ESP_OK) {
    return 4;
  }
  if (i2c_master_cmd_begin(PORT_I2C, cmd, 1000) != ESP_OK) {
    return 5;
  }
  i2c_cmd_link_delete(cmd);

  return 0;
}
int writeMaster(const char *s) {
  return writeMaster((const uint8_t *)s, strlen(s));
}
int writeMaster(const String &s) {
  return writeMaster((const uint8_t *)s.c_str(), s.length());
}

/*
String readMaster(const uint8_t *donnees = NULL, size_t taille = 0) {
  char tampon[TAILLE_TAMPON];
  for (int i = 0; i < TAILLE_TAMPON; i++) {
    tampon[i] = 0;
  }

  int e;

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  //if (donnees == NULL || taille == 0) {
  //  i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_WRITE, true);
  //  i2c_master_write(cmd, (uint8_t *)donnees, taille, true);
  //  i2c_master_start(cmd);
  //}

  if ((e = i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_READ,
                                 true)) != ESP_OK) {
    return String("master write byte (address)");
  }

  //if (i2c_master_read(cmd, (uint8_t *)&tampon, TAILLE_TAMPON - 1,
  //                    I2C_MASTER_LAST_NACK) != ESP_OK) {
  //  return String("master read (data)");
  //} // I2C_MASTER_LAST_NACK


  if (i2c_master_read_byte(cmd, (uint8_t *)&tampon, I2C_MASTER_ACK) != ESP_OK) {
    return String("master read byte (data)");
  } // I2C_MASTER_LAST_NACK

  //if (i2c_master_stop(cmd) != ESP_OK) {
    //return String("master stop");
  //}

  if ((e = i2c_master_cmd_begin(PORT_I2C, cmd, 100000 / portTICK_RATE_MS)) !=
      ESP_OK) {
    int a[] = {
        ESP_OK,          ESP_ERR_INVALID_ARG, ESP_FAIL, ESP_ERR_INVALID_STATE,
        ESP_ERR_TIMEOUT,
    };
    (void)a;
    return String("master cmd begin: ") + e;
  }

  i2c_cmd_link_delete(cmd);

  return String(tampon);
}*/

String readMaster(const uint8_t *donnees = NULL, size_t taille = 0) {
  char tampon[TAILLE_TAMPON];
  for (int i = 0; i < TAILLE_TAMPON; i++) {
    tampon[i] = 0;
  }

  int e;

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  //if (donnees == NULL || taille == 0) {
  //  i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_WRITE, true);
  //  i2c_master_write(cmd, (uint8_t *)donnees, taille, true);
  //  i2c_master_start(cmd);
  //}

  if ((e = i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_READ,
                                 true)) != ESP_OK) {
    return String("master write byte (address)");
  }

  //if (i2c_master_read(cmd, (uint8_t *)&tampon, TAILLE_TAMPON - 1,
  //                    I2C_MASTER_LAST_NACK) != ESP_OK) {
  //  return String("master read (data)");
  //} // I2C_MASTER_LAST_NACK


  if (i2c_master_read_byte(cmd, (uint8_t *)&tampon, I2C_MASTER_ACK) != ESP_OK) {
    return String("master read byte (data)");
  } // I2C_MASTER_LAST_NACK

  //if (i2c_master_stop(cmd) != ESP_OK) {
    //return String("master stop");
  //}

  if ((e = i2c_master_cmd_begin(PORT_I2C, cmd, 100000 / portTICK_RATE_MS)) !=
      ESP_OK) {
    int a[] = {
        ESP_OK,          ESP_ERR_INVALID_ARG, ESP_FAIL, ESP_ERR_INVALID_STATE,
        ESP_ERR_TIMEOUT,
    };
    (void)a;
    return String("master cmd begin: ") + e;
  }

  i2c_cmd_link_delete(cmd);

  return String(tampon);
}

String readMaster(const char *s) {
  return readMaster((const uint8_t *)s, strlen(s));
}
String readMaster(const String &s) {
  return readMaster((const uint8_t *)s.c_str(), s.length());
}

void setup() {
  Serial.begin(9600);

  int i = initI2C(I2C_MODE_MASTER);
  // initI2C(I2C_MODE_SLAVE); // Initialiser le port I2C en mode esclave.
  Serial.print("Erreur initialisation: ");
  Serial.println(ERREURS_INIT[i]);

  /*pinMode(SDA, INPUT);
  int compte = 0;
  while (true) {
    // Serial.print(digitalRead(SDA));
    while (digitalRead(SDA) == 1)
      ;
    while (digitalRead(SDA) == 0)
      ;
    Serial.println(compte);
    compte += 1;
  }*/
}

volatile int nbMsgs = 0;
int msg = 0;
int nbPrecedent = 0;
void loop() {
  /*if (Serial.available() != 0) {
    // Lire une ligne entrée par l'utilisateur.
    String message = Serial.readStringUntil('\n');
    if (message.startsWith("lire")) {
      // initI2C(I2C_MODE_MASTER);

      readMaster(message);

      // initI2C(I2C_MODE_SLAVE);
    } else if (message.startsWith("ecrire")) {
      // initI2C(I2C_MODE_MASTER);
      Serial.print("Ecrire: ");
      Serial.println(message);
      writeMaster(message);

      // initI2C(I2C_MODE_SLAVE);
    } else {
      Serial.print("Prochaines réponses: ");
      Serial.println(message);
      reponse = message;
    }
  }*/

  /*if (nbMsgs > nbPrecedent) {
    nbPrecedent++;
    Serial.print("Message recu: ");
    Serial.println(msg);
  }*/

  if (Serial.available()) {
    String msg = Serial.readStringUntil('\n');
    if (msg.startsWith("lire ")) {
      msg = msg.substring(5);

      String recu;
      if (msg.length() > 0) {
        Serial.print("Envoyer message: ");
        Serial.println(msg);
        recu = readMaster(msg);
      } else {
        recu = readMaster();
      }

      Serial.print("Message reçu: ");
      Serial.println(recu);
    } else if (msg.startsWith("ecrire ")) {
      msg = msg.substring(7);

      Serial.print("Envoyer message: ");
      Serial.println(msg);

      int erreur = writeMaster(msg);
      Serial.print("Erreur écriture: ");
      Serial.println(ERR_WRITE[erreur]);
    } else {
      reponse = msg;
    }
  }
}

/**
 * @brief Reçoit un message du port I²C et l'envoi sur le port sériel.
 */
void IRAM_ATTR onReceive(void *arg) {
  char tampon[TAILLE_TAMPON];
  tampon[TAILLE_TAMPON - 1] = 0;

  nbMsgs++;

  uint8_t rw = 0;
  i2c_slave_read_buffer(PORT_I2C, &rw, 1, 0);
  if ((rw & 1) != I2C_MASTER_READ) {
    int taille = i2c_slave_read_buffer(PORT_I2C, (uint8_t *)&tampon,
                                       TAILLE_TAMPON - 1, 0);
    if (taille >= 0) {
      tampon[taille] = 0;
      // Serial.print("Message reçu: ");
      // Serial.println(tampon);
    } else {
      // Serial.println(
      //"Une erreur est survenue lors de la réception d'un message");
    }
  } else {
    i2c_slave_write_buffer(PORT_I2C, (uint8_t *)reponse.c_str(),
                           reponse.length(), 0);
    // Serial.println("Réponse à un message envoyée");
  }
}
