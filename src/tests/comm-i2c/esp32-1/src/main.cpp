/**
 * @brief
 *
 * https://github.com/RoboticsBrno/krouzek_esp32/blob/a58de2e8398fffb01415e596f107b699bbfbbdde/20191114_i2c/slave.cpp
 * https://github.com/InfiniteYuan1/ESP32-I2C-Slave/blob/master/main/i2c_example_main.c
 */

#include <Arduino.h>
#include <driver/i2c.h>
#include <soc/i2c_struct.h>

void onReceive(void *);
void onRequest();

String reponse = "Message non initialisé";
intr_handle_t handleIsr;

const int ADRESSE_I2C = 20;
const int ADRESSE_PAIR = 8;
#define PORT_I2C I2C_NUM_0
#define TAILLE_TAMPON 128 // 128 bytes

static DRAM_ATTR i2c_dev_t *const PORT = &I2C0;
static DRAM_ATTR volatile int nbInts = 0;

const char *ERREURS_INIT[] = {
    "Aucune",       "Driver delete", "free isr",
    "register isr", "config params", "driver install",
};

int initI2C(i2c_mode_t mode) {
  i2c_config_t config;

  int prev_mode = -1;

  if (prev_mode != -1) {
    if (i2c_driver_delete(PORT_I2C) != ESP_OK) {
      return 1;
    }

    if (i2c_isr_free(handleIsr) != ESP_OK) {
      return 2;
    }
  }
  prev_mode = mode;

  // Configurer le mode.
  config.mode = mode;

  // Configurer les pins.
  config.sda_io_num = GPIO_NUM_21;
  config.sda_pullup_en = GPIO_PULLUP_ENABLE;
  config.scl_io_num = GPIO_NUM_22;
  config.scl_pullup_en = GPIO_PULLUP_ENABLE;

  if (mode == I2C_MODE_SLAVE) {
    config.slave.addr_10bit_en = false;
    config.slave.slave_addr = ADRESSE_I2C;
  } else {
    config.master.clk_speed = 1000; // 1kHz
  }

  if (i2c_param_config(PORT_I2C, &config) != ESP_OK) {
    return 4;
  }
  if (i2c_driver_install(PORT_I2C, mode, TAILLE_TAMPON, TAILLE_TAMPON, 0) !=
      ESP_OK) {
    return 5;
  }

  if (i2c_isr_register(PORT_I2C, onReceive, NULL, 0, &handleIsr) != ESP_OK) {
    return 3;
  }

  return 0;
}

const char *ERR_WRITE[] = {
    "Aucune erreur",       "master start", "master write byte (address)",
    "master write (data)", "master stop",  "master cmd begin",
};
int writeMaster(const uint8_t *donnees, size_t taille) {
  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  if (i2c_master_start(cmd) != ESP_OK) {
    return 1;
  }

  if (i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_WRITE,
                            true) != ESP_OK) {
    return 2;
  }
  if (i2c_master_write(cmd, (uint8_t *)donnees, taille, true) != ESP_OK) {
    return 3;
  }
  if (i2c_master_stop(cmd) != ESP_OK) {
    return 4;
  }
  if (i2c_master_cmd_begin(PORT_I2C, cmd, 1000) != ESP_OK) {
    return 5;
  }
  i2c_cmd_link_delete(cmd);

  return 0;
}

/*
String readMaster(const uint8_t *donnees = NULL, size_t taille = 0) {
  static const int taille_tampon = 60;
  char tampon[taille_tampon];
  for (int i = 0; i < taille_tampon; i++) {
    tampon[i] = 0;
  }

  int e;

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  if (donnees == NULL || taille == 0) {
    i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write(cmd, (uint8_t *)donnees, taille, true);
    i2c_master_start(cmd);
  }

  if ((e = i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_READ,
                                 true)) != ESP_OK) {
    return String("master write byte (address)");
  }

  if (i2c_master_read(cmd, (uint8_t *)&tampon, taille_tampon - 1,
                      I2C_MASTER_ACK) != ESP_OK) {
    return String("master read (data)");
  } // I2C_MASTER_LAST_NACK

  if (i2c_master_stop(cmd) != ESP_OK) {
    return String("master stop");
  }

  if ((e = i2c_master_cmd_begin(PORT_I2C, cmd, 10000 / portTICK_RATE_MS)) !=
  //if ((e = i2c_master_cmd_begin(PORT_I2C, cmd, 10000 / portTICK_RATE_MS)) !=
      ESP_OK) {
    return String("master cmd begin: ") + e;
  }

  i2c_cmd_link_delete(cmd);

  return String(tampon);
}*/

String readMaster(const uint8_t *donnees = NULL, size_t taille = 0) {
  static const int taille_tampon = 60;
  char tampon[taille_tampon];
  for (int i = 0; i < taille_tampon; i++) {
    tampon[i] = 0;
  }

  int e;

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();

  i2c_master_start(cmd);
  if (donnees == NULL || taille == 0) {
    i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write(cmd, (uint8_t *)donnees, taille, true);
    i2c_master_start(cmd);
  }

  if ((e = i2c_master_write_byte(cmd, (ADRESSE_PAIR << 1) | I2C_MASTER_READ,
                                 true)) != ESP_OK) {
    return String("master write byte (address)");
  }

  int nbLu = 0;
  while (nbLu < taille && e == ESP_OK) {
    i2c_master_read_byte(cmd, (uint8_t *)&tampon[nbLu], I2C_MASTER_ACK);
    if ((e = i2c_master_cmd_begin(PORT_I2C, cmd, 100 / portTICK_RATE_MS)) !=
        ESP_OK) {
      return String("master cmd begin: ") + e + ", " + nbLu;
    }

    i2c_cmd_link_delete(cmd);
    cmd = i2c_cmd_link_create();

    nbLu++;
  }

  if (i2c_master_stop(cmd) != ESP_OK) {
    return String("master stop");
  }

  if ((e = i2c_master_cmd_begin(PORT_I2C, cmd, 100 / portTICK_RATE_MS)) !=
      ESP_OK) {
    return String("master cmd begin iii: ") + tampon + ", " + e;
  }

  i2c_cmd_link_delete(cmd);

  return String(tampon);
}

void setup() {
  Serial.begin(115200);

  int i = initI2C(I2C_MODE_MASTER);
  //int i = initI2C(I2C_MODE_SLAVE); // Initialiser le port I2C en mode esclave.
  Serial.print("Erreur initialisation: ");
  Serial.println(ERREURS_INIT[i]);
}

volatile int nbMsgs = 0;
int msg = 0;
int nbPrecedent = 0;
void loop() {
  if (Serial.available()) {
    String msg = Serial.readStringUntil('\n');
    if (msg.startsWith("lire ")) {
      msg = msg.substring(5);

      String recu;
      if (msg.length() > 0) {
        Serial.print("Envoyer message: ");
        Serial.println(msg);
        recu = readMaster((const uint8_t *)msg.c_str(), msg.length());
      } else {
        recu = readMaster();
      }

      Serial.print("Message reçu: ");
      Serial.println(recu);
    } else if (msg.startsWith("ecrire ")) {
      msg = msg.substring(7);

      Serial.print("Envoyer message: ");
      Serial.println(msg);

      int erreur = writeMaster((const uint8_t *)msg.c_str(), msg.length());
      Serial.print("Erreur écriture: ");
      Serial.println(ERR_WRITE[erreur]);
    } else {
      reponse = msg;
    }
  }
}

/**
 * @brief Reçoit un message du port I²C et l'envoi sur le port sériel.
 */
void IRAM_ATTR onReceive(void *arg) { nbInts++; }
