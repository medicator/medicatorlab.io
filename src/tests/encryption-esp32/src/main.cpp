/**
 * @brief Test la communication I²C en tant que maître et en tant qu'esclave.
 *
 * Par défaut le contrôleur est en mode esclave et est prêt à accepter des
 * messages. Il ne se met en mode maître que lorsque nécessaire.
 *
 * Lorsqu'il est en mode esclave (à l'adresse 9), il affiche les données qu'il
 * reçoit sur le port sériel et répond aux requêtes de données avec un message
 * qui peut être configuré.
 *
 * Lorsqu'il reçoit 'ecrire <msg>' sur son port sériel, le contrôleur se
 * configure en tant que maître et envoi le message donné à l'esclave à
 * l'adresse ADRESSE_PAIR.
 *
 * Lorsqu'il reçoit 'lire <msg>', le contrôleur se configure en tant que maître
 * et envoi le message donné à l'esclave à l'adresse ADRESSE_PAIR puis lit sa
 * réponse et l'affiche.
 *
 * Lorsqu'il reçoit un message formatté de n'importe quelle autre façon, le
 * message donné est utilisé pour répondre aux prochaines requêtes qui lui sont
 * envoyées lorsqu'il est en mode esclave.
 */

#include <Arduino.h>
#include <Esp32Wire/Master.h>
#include <Esp32Wire/Slave.h>
#include <hwcrypto/aes.h>
#include <rom/crc.h>

//! Taille de la clé AES 256 en nombre de bits
#define TAILLE_CLE_BITS 256
//! Taille de la clé d'encryption AES 256
#define TAILLE_CLE (TAILLE_CLE_BITS / 8)
//! Taille d'un bloc pour l'encryption/décryption AES
#define TAILLE_BLOC (128 / 8)
//! Taille du vecteur d'initialisation AES
#define TAILLE_IV 16
//! Taille des données encryptées pour la structure TMsg
#define TAILLE_DONNEES (10 * TAILLE_BLOC)
//! Taille d'un paquet transféré sur le port I²C
#define TAILLE_SEGMENT 32

//! Message transféré sur le port I²C
struct TMsg {
    //! Clé d'encryption AES 256
    uint8_t cle[TAILLE_CLE];
    //! Vecteur d'initialisation
    uint8_t iv[TAILLE_IV];
    //! Données encryptées
    uint8_t donnees[TAILLE_DONNEES];
    //! Code CRC (little endian) qui s'applique à toutes les données de la
    //! structure avant qu'elles soient encryptées, sauf à lui-même
    uint32_t crc;
} msg;

//! Message décryté transféré.
static char message[TAILLE_DONNEES];

//! Adresse de ce contrôleur en mode esclave.
static const int ADRESSE = 9;
//! Adresse de l'esclave.
static const int ADRESSE_PAIR = 8;

//! Port 0 du contrôleur I²C en tant que maître.
static Master maitre(0);
//! Port 0 du contrôleur I²C en tant qu'esclave.
static Slave slave(0);

void onReceive(int bytes);
void preparerMsg();

void setup() {
    Serial.begin(115200);

    slave.begin(ADRESSE);
    slave.onReceive(onReceive);

    Serial.println("Initialisation terminée");
}

void loop() {
    // Lire une ligne entrée par l'utilisateur.
    char c;
    int i = 0;
    do {
        // Attendre le prochain caractère.
        while (Serial.available() == 0) {
        }

        c = Serial.read();
        Serial.print(c);
        message[i] = c;
        i++;
    } while (c != '\n');
    message[i - 2] = 0;

    slave.stop();
    Serial.println(maitre.begin());

    Serial.print("Envoi du message: ");
    Serial.println(message);

    // Envoyer le message.
    preparerMsg();
    for (size_t i = 0; i < sizeof(TMsg); i += TAILLE_SEGMENT) {
        int taille = sizeof(TMsg) - i;
        if (taille > TAILLE_SEGMENT) {
            taille = TAILLE_SEGMENT;
        }

        maitre.write(ADRESSE_PAIR, &((uint8_t *)&msg)[i], taille);
    }

    // Recevoir la réponse au message.
    int reponse = maitre.read(ADRESSE_PAIR);
    Serial.print("Réponse: ");
    Serial.println(reponse);

    maitre.stop();
    slave.begin(ADRESSE);
    slave.onReceive(onReceive);
}

void preparerMsg() {
    // Générer une clé aléatoire.
    esp_fill_random(
        msg.cle,
        TAILLE_CLE); // Ceci devient réellement aléatoire à partir du moment où
                     // le WiFi ou le Bluetooth est activé. Avant cela, les
                     // nombres générés sont pseudo aléatoires.

    // Générer un IV aléatoire.
    uint8_t iv[TAILLE_IV];
    esp_fill_random(iv, TAILLE_IV);
    memcpy(msg.iv, iv, TAILLE_IV);

    // Copier le message dans la structure de message (ceci est nécessaire pour
    // le calcul du CRC).
    memcpy(msg.donnees, message, TAILLE_DONNEES);

    // Calculer le CRC.
    msg.crc = crc32_le(0, (uint8_t *)&msg, sizeof(TMsg) - sizeof(uint32_t));

    Serial.println();
    Serial.println();
    for (int i = 0; i < sizeof(TMsg); i++) {
        uint8_t c = ((uint8_t *)&msg)[i];
        int j = (int)c;
        Serial.printf("0x%.2x,", j);
    }
    Serial.println();
    Serial.println();

    // Encrypter les données.
    esp_aes_context ctx;
    esp_aes_init(&ctx);
    esp_aes_setkey(&ctx, msg.cle, TAILLE_CLE_BITS);
    int e = esp_aes_crypt_cbc(&ctx, ESP_AES_ENCRYPT, TAILLE_DONNEES, iv,
                      (uint8_t *)message, (uint8_t *)msg.donnees);
    esp_aes_free(&ctx);

    //int e = ERR_AES_INVALID_INPUT_LENGTH;

    Serial.print("Erreur encryption: ");
    Serial.println(e);

    Serial.println();
    Serial.println();
    for (int i = 0; i < sizeof(TMsg); i++) {
        uint8_t c = ((uint8_t *)&msg)[i];
        int j = (int)c;
        Serial.printf("0x%.2x,", j);
    }
    Serial.println();
    Serial.println();
}

static char message2[TAILLE_DONNEES];
static uint8_t iv2[TAILLE_IV];

/**
 * @brief Reçoit un message sur le port I²C et l'envoi sur le port sériel
 */
void onReceive(
    //! Nombre d'octets reçus
    int octets) {
    int debut = micros();
    if (slave.available() >= sizeof(TMsg)) {
        slave.read((uint8_t *)&msg, sizeof(TMsg));

        // Copier les données du message dans un autre tampon (ceci est
        // nécessaire pour pouvoir le passer à la fonction de décryption).
        memcpy(message2, msg.donnees, TAILLE_DONNEES);
        memcpy(iv2, msg.iv, TAILLE_IV);

        // Décrypter les données.
        esp_aes_context ctx;
        esp_aes_init(&ctx);
        esp_aes_setkey(&ctx, msg.cle, TAILLE_CLE_BITS);
        esp_aes_crypt_cbc(&ctx, ESP_AES_DECRYPT, TAILLE_DONNEES, iv2,
                          (uint8_t *)message2, (uint8_t *)msg.donnees);
        esp_aes_free(&ctx);

        // Calculer le CRC.
        int crc = crc32_le(0, (uint8_t *)&msg, sizeof(TMsg) - sizeof(uint32_t));

        slave.resetTxBuffer();
        slave.write((uint8_t)(crc == msg.crc ? '1' : '0'));

        int fin = micros();
        Serial.print("Durée de réception: ");
        Serial.println(fin - debut);

        Serial.print("Message valide: ");
        Serial.println(crc == msg.crc);

        // Afficher le message reçu.
        msg.donnees[TAILLE_DONNEES - 1] = 0;
        Serial.print("Message reçu: ");
        Serial.println((char *)msg.donnees);
    }
}
