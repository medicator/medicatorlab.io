/**
 * @brief I²C Slave class declaration
 */
#ifndef Slave_H
#define Slave_H

#include "i2c.h"
#include <stdint.h>

//! I²C slave input and output buffer size
#define SLAVE_BUF_LEN 1024

//! I²C hardware Slave
class Slave {
private:
    //! I²C port number
    i2c_port_t port;
    //! Whether the begin function has been called and hasn't returned any error
    bool started = false;
    //! Timeout in ms for reading from and writing to the slave's buffer
    int timeout = 100;

public:
    /**
     * @brief Initializes the default properties for the I²C slave, but doesn't
     *        start it. You need to call the `begin` method before doing
     *        anything else.
     */
    Slave(
        //! Port of the hardware I²C controller to use. Either 0 or 1.
        int port);

    /**
     * @brief Setup an I²C slave with the given address
     */
    int begin(
        //! I²C Address
        uint16_t address);

    /**
     * @brief Deletes the I²C driver and removes the registered onReceive and
     *        onRequest handlers
     * @note Call this function whenever your need to use the port as an I²C
     *       master
     */
    int stop();

    /**
     * @brief
     * @note not implemented
     */
    void onRequest(
        //! handler to register
        void (*fn)(void));

    /**
     * @brief Register a handler called whenever data is received from the
     * master.
     * @note Pass NULL as parameter to remove the previous handler. If you call
     *       this function multiple times, the last passed value will be used.
     */
    void onReceive(
        //! handler to register
        void (*fn)(int));

    /**
     * @brief Writes the given byte to the slave's tx buffer
     *
     * @return an error code
     * @retval 0 the byte wasn't written to the buffer
     * @retval 1 the byte was written to the buffer
     * @retval -1 parameter error
     */
    int write(
        //! Byte to write
        uint8_t byte);
    /**
     * @brief Writes the given bytes to the slave's tx buffer
     *
     * @return number of bytes written to the buffer
     * @retval >=0 number of bytes that were written
     * @retval -1 parameter error
     */
    int write(
        //! Bytes to send
        const uint8_t *bytes,
        //! Number of bytes to send
        int size);
    /**
     * @brief Writes the given string to the slave's tx buffer
     *
     * @return number of bytes written to the buffer
     * @retval >=0 number of bytes that were written
     * @retval -1 parameter error
     */
    int write(
        //! Null terminated string
        const char *string);

    /**
     * @brief Reset the software and hardware tx buffers
     */
    void resetTxBuffer();

    /**
     * @brief Reads a single byte from the slave buffer and returns it.
     *
     * @return The byte that was read
     * @retval >=0 The byte that was read
     * @retval -1 An error occured
     */
    int read();
    /**
     * @brief Reads up to max_size bytes from the slave's buffer
     *
     * @return The number of bytes read
     * @retval >=0 The number of bytes read
     * @retval -1 An error occured
     */
    int read(
        //! Buffer to write the bytes that are read
        uint8_t *bytes,
        //! Maximum number of bytes to read
        int max_size);

    /**
     * @brief returns the number of bytes available to read
     *
     * @return The number of bytes available to read
     */
    int available();

    /**
     * @brief sets the timeout for reading from and writing to the slave's
     * buffer
     * @note the default value is 100ms
     */
    void setTimeout(
        //! Timeout in ms
        int timeout);

    /**
     * Calls Slave::stop()
     */
    ~Slave();
};

#endif /* Slave_H */
