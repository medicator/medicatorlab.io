#include "Master.h"
#include <string.h>

Master::Master(int port) {
    this->port = (i2c_port_t)port;
}

int Master::begin() {
    // Error code.
    int e;

    // Configure the port.
    i2c_config_t config;
    config.mode = I2C_MODE_MASTER;
    config.sda_io_num = GPIO_NUM_21;
    config.sda_pullup_en = GPIO_PULLUP_ENABLE;
    config.scl_io_num = GPIO_NUM_22;
    config.scl_pullup_en = GPIO_PULLUP_ENABLE;
    config.master.clk_speed = 10000; // 100kHz
    if ((e = i2c_param_config(port, &config)) != ESP_OK) {
        return e;
    }

    // Install the driver.
    e = i2c_driver_install(port, I2C_MODE_MASTER, 0, 0, 0);

    if (e == ESP_OK) {
        started = true;
    }

    return e;
}

/**
 * @brief
 */
static esp_err_t sendAddress(
    //! Command handle
    i2c_cmd_handle_t cmd,
    //! Slave address
    uint16_t address,
    //! Whether to read or to write to the slave.
    //! true: read.
    //! false: write.
    bool rw,
    //! 2 buffer long buffer where to store the data for the cmd
    uint8_t *buf) {
    // Error code.
    esp_err_t e = ESP_ERR_INVALID_ARG;
    uint8_t rwMask = rw ? 1 : 0;

    if (address < 128) { // 7 bit mode
        uint8_t data = ((uint8_t)address << 1) | rwMask;
        e = i2c_master_write_byte(cmd, data, true);
    } else if (address < 1024 && buf != NULL) { // 10 bit mode
        buf[0] = (address >> 7) & (0b110 | rwMask);
        buf[1] = (uint8_t)address;
        e = i2c_master_write(cmd, buf, 2, true);
    }

    return e;
}

int Master::write(uint16_t address, uint8_t byte, bool stop) {
    esp_err_t e = 0;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    // Send the start bit.
    e = i2c_master_start(cmd);

    // Send the address.
    if (e == ESP_OK) {
        uint8_t addrBuf[2];
        e = sendAddress(cmd, address, false, addrBuf);
    }

    // Send the data.
    if (e == ESP_OK) {
        e = i2c_master_write_byte(cmd, byte, true);
    }

    // Send the data.
    if (e == ESP_OK && stop) {
        e = i2c_master_stop(cmd);
    }

    // Execute the commands.
    if (e == ESP_OK) {
        e = i2c_master_cmd_begin(port, cmd, timeout / portTICK_RATE_MS);
    }

    i2c_cmd_link_delete(cmd);
    return e;
}
int Master::write(uint16_t address, const uint8_t *buffer, int size, bool stop) {
    esp_err_t e = 0;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    // Send the start bit.
    e = i2c_master_start(cmd);

    // Send the address.
    if (e == ESP_OK) {
        uint8_t addrBuf[2];
        e = sendAddress(cmd, address, false, addrBuf);
    }

    // Send the data.
    if (e == ESP_OK) {
        e = i2c_master_write(cmd, (uint8_t*)buffer, size, true);
    }

    // Send the data.
    if (e == ESP_OK && stop) {
        e = i2c_master_stop(cmd);
    }

    // Execute the commands.
    if (e == ESP_OK) {
        e = i2c_master_cmd_begin(port, cmd, timeout / portTICK_RATE_MS);
    }

    i2c_cmd_link_delete(cmd);
    return e;
}
int Master::write(uint16_t address, const char *string, bool stop) {
    if (string != NULL) {
        return write(address, (const uint8_t *)string, strlen(string), stop);
    } else {
        return -1;
    }
}

int Master::read(uint16_t address, bool stop) {
    esp_err_t e = 0;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    // Send the start bit.
    e = i2c_master_start(cmd);

    // Send the address.
    if (e == ESP_OK) {
        uint8_t addrBuf[2];
        e = sendAddress(cmd, address, true, addrBuf);
    }

    // Read the data.
    uint8_t data;
    if (e == ESP_OK) {
        e = i2c_master_read_byte(cmd, &data, I2C_MASTER_LAST_NACK);
    }

    // Send the data.
    if (e == ESP_OK && stop) {
        e = i2c_master_stop(cmd);
    }

    // Execute the commands.
    if (e == ESP_OK) {
        e = i2c_master_cmd_begin(port, cmd, timeout / portTICK_RATE_MS);
    }

    i2c_cmd_link_delete(cmd);

    if (e == ESP_OK) {
        e = data;
    } else if (e != -1) {
        e = -e;
    }
    return e;
}
int Master::read(uint16_t address, uint8_t *buffer, int size, bool stop) {
    esp_err_t e = 0;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    // Send the start bit.
    e = i2c_master_start(cmd);

    // Send the address.
    if (e == ESP_OK) {
        uint8_t addrBuf[2];
        e = sendAddress(cmd, address, true, addrBuf);
    }

    // Read the data.
    if (e == ESP_OK) {
        e = i2c_master_read(cmd, buffer, size, I2C_MASTER_LAST_NACK);
    }

    // Send the stop bit.
    if (e == ESP_OK && stop) {
        e = i2c_master_stop(cmd);
    }

    // Execute the commands.
    if (e == ESP_OK) {
        e = i2c_master_cmd_begin(port, cmd, timeout / portTICK_RATE_MS);
    }

    i2c_cmd_link_delete(cmd);

    return e;
}

int Master::stop() {
    if (started) {
        started = false;
        return i2c_driver_delete(port);
    } else {
        return -1;
    }
}

Master::~Master() {
    stop();
}
