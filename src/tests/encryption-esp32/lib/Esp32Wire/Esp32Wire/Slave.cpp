#include "Slave.h"
#include <string.h>

Slave::Slave(int port) {
    this->port = (i2c_port_t)port;
}

int Slave::begin(uint16_t address) {
    // Error code.
    int e;

    // Configure the port.
    i2c_config_t config;
    config.mode = I2C_MODE_SLAVE;
    config.sda_io_num = GPIO_NUM_21;
    config.sda_pullup_en = GPIO_PULLUP_ENABLE;
    config.scl_io_num = GPIO_NUM_22;
    config.scl_pullup_en = GPIO_PULLUP_ENABLE;
    config.slave.addr_10bit_en = address >= 128;
    config.slave.slave_addr = address;
    if ((e = i2c_param_config(port, &config)) != ESP_OK) {
        return e;
    }

    // Install the driver.
    e = i2c_driver_install(port, I2C_MODE_SLAVE, 512, 512, 0);

    if (e == ESP_OK) {
        started = true;
    }

    return e;
}

void Slave::onReceive(void (*fn)(int)) {
    i2c_slave_register_on_receive(port, fn);
}

int Slave::write(uint8_t byte) {
    return i2c_slave_write_buffer(port, &byte, 1, timeout / portTICK_RATE_MS);
}
int Slave::write(const uint8_t *bytes, int size) {
    if (bytes != NULL) {
        return i2c_slave_write_buffer(port, (uint8_t *)bytes, size,
                                      timeout / portTICK_RATE_MS);
    } else {
        return -1;
    }
}
int Slave::write(const char *string) {
    if (string != NULL) {
        return i2c_slave_write_buffer(port, (uint8_t *)string, strlen(string),
                                      timeout / portTICK_RATE_MS);
    } else {
        return -1;
    }
}

void Slave::resetTxBuffer() {
    i2c_reset_tx_fifo(port);
    i2c_reset_soft_tx_fifo(port);
}

int Slave::read() {
    uint8_t byte;
    int i = i2c_slave_read_buffer(port, &byte, 1, timeout / portTICK_RATE_MS);
    return i == 1 ? byte : i;
}
int Slave::read(uint8_t *bytes, int max_size) {
    return i2c_slave_read_buffer(port, bytes, max_size,
                                 timeout / portTICK_RATE_MS);
}

int Slave::available() {
    return i2c_slave_read_available(port);
}

void Slave::setTimeout(int timeout) {
    this->timeout = timeout;
}

int Slave::stop() {
    if (started) {
        started = false;

        onReceive(NULL);
        i2c_reset_rx_fifo(port);
        i2c_reset_tx_fifo(port);
        return i2c_driver_delete(port);
    } else {
        return -1;
    }
}

Slave::~Slave() {
    stop();
}
