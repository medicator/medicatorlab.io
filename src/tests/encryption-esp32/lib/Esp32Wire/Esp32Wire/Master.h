/**
 *
 */
#ifndef Master_H
#define Master_H

#include "i2c.h"
#include <stdint.h>

//! I²C master input and output buffer size
#define SLAVE_BUF_LEN 1024

//! Master on I²C bus 0
class Master {
private:
    //! I²C port number
    i2c_port_t port;
    //! Whether the begin function has been called and hasn't returned any error
    bool started = false;
    //! Timeout in ms for reading from and writing to the master's buffer
    int timeout = 100;

public:
    /**
     * @brief Initializes the default properties for the I²C master, but doesn't
     *        start it. You need to call the `begin` method before doing
     *        anything else.
     *
     */
    Master(int port);

    /**
     * @brief Setup an I²C master with the given address
     */
    int begin();

    /**
     * @brief Deletes the I²C driver and removes the registered onReceive and
     *        onRequest handlers
     * @note Call this function whenever your need to use the port as an I²C
     *       master
     */
    int stop();

    /**
     * @brief Send a byte to the given slave
     * @return an error code
     */
    int write(
        //! Slave's address
        uint16_t address,
        //! Byte to send
        uint8_t byte,
        //! Whether to send the stop bit at the end of the communication
        bool stop = true);
    /**
     * @brief Send bytes to the given slave
     * @return an error code
     */
    int write(
        //! Slave's address
        uint16_t address,
        //! Bytes to sends
        const uint8_t *bytes,
        //! Number of bytes to send
        int size,
        //! Whether to send the stop bit at the end of the communication
        bool stop = true);
    /**
     * @brief Send a string to the given slave
     * @return an error code
     */
    int write(
        //! Slave's address
        uint16_t address,
        //! String to send
        const char *string,
        //! Whether to send the stop bit at the end of the communication
        bool stop = true);

    /**
     * @brief Reads a single byte from the given slave
     */
    int read(uint16_t address, bool stop = true);
    /**
     * @brief Reads bytes from a slave into a buffer
     */
    int read(uint16_t address, uint8_t *buffer, int size, bool stop = true);

    /**
     * @brief set the timeout for reading from and writing to the master's
     * buffer
     * @note the default value is 100ms
     */
    int setTimeout(
        //! Timeout in ms
        int timeout);

    /**
     * Calls Master::stop()
     */
    ~Master();
};

#endif /* Master_H */
