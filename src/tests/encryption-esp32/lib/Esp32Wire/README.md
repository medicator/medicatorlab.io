# ESP32 I²C Master and Slave Library
[Arduino](https://www.arduino.cc/) library that implements an [I²C] master and an [I²C] slave for the [ESP32 microcontroller][ESP32]. This library only depends on [esp-idf] and never include `Arduino.h`, therefore it could probably be tweaked a little and used in [esp-idf] projects.

Instead of completely reinventing the wheel like the [arduino-esp32] does, this uses the [esp-idf] [I²C] driver with a few tweaks to have `onReceive`. Unlike the [arduino-esp32], this library can also be used to make an [I²C] slave. However, it isn't compatible with [Arduino Wire] library at this time.

The full technical documentation can be accessed [here](https://zakcodes.com/Esp32Wire).

[I²C]: https://i2c.info/
[ESP32]: https://www.espressif.com/en/products/hardware/esp32/overview
[esp-idf]: https://github.com/espressif/esp-idf
[arduino-esp32]: https://github.com/espressif/arduino-esp32
[Arduino Wire]: https://www.arduino.cc/en/Reference/Wire
