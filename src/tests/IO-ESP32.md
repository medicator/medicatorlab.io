# IO du ESP32

## But
S'assurer que toutes les entrées et sorties analogiques et digitales de mes 2 ESP32 fonctionnent et par la même occasion s'assurer qu'on peut les programmer.

## Hypothèse

## Démarche

## Résultats

## Analyse

## Conclusion
Il faudra faire attention [aux modes démarrage du ESP32](https://github.com/espressif/esptool/wiki/ESP32-Boot-Mode-Selection), car nous avons eu des problèmes pour un autre projet avec ceux du ESP8266. Lorsque nous allumions le ESP8266 et que certaines broches étaient à des états particuliers, le ESP8266 démarrait dans un mode où il n'exécutait pas le programme que nous avions mis dessus, mais un autre. Comme j'ai beaucoup plus d'entrées et sorties que j'en ai besoin sur ce modèle, je vais éviter d'utiliser les broches qui changent le mode de démarrage de ce microcontrôleur.
