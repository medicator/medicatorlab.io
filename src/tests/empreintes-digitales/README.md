# Authentification d'empreintes digitales

## But
Pouvoir enrôler une ou plusieurs empreintes digitales et pouvoir les authentifier.

## Hypothèse
* tutoriel: https://cdn-learn.adafruit.com/downloads/pdf/adafruit-optical-fingerprint-sensor.pdf
* librarie: https://github.com/adafruit/Adafruit-Fingerprint-Sensor-Library/

## Résultats

## Analyse

## Problèmes
Je ne peux pas appeler la fonction `finger.begin(57600);`, car elle initialise mon port sériel avec les pins par défaut qui ne sont pas celles que j'utilise. Je peux cependant remplacer l'appel à cette fonction par ce code:
```
delay(1000);
mySerial.begin(57600, SERIAL_8N1, RX, TX);
```
car il est le contenu de la fonction, mais j'y configure les pins dont j'ai besoin.

Pour trouver le bug, j'ai fait le code une première fois qui ne fonctionnait pas sur le ESP32, puis j'ai utiliser l'exemple pour le UNO et je l'ai fait sur le UNO et il a fonctionné. J'ai donc inspecté le contenu de la fonction et j'y ait découvert le problème.

## Conclusion
La lumière verte est vraiment dérangeante, donc il faudrait ajouter un circuit pour enlever l'alimentation du capteur, puisqu'on en a seulement besoin lorsqu'il est l'heure de distribuer un médicament.
