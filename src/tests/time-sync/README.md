# Synchronisation de l'heure

## Problèmes survenus
La seule manière pour updater l'heure donnée par le Device OS API est d'utiliser le service `Particle.syncTime` qui utilise le cloud de Particle. Comme je ne compte pas utiliser leur cloud ou leur carte SIM intégrée, je ne pourrais pas utiliser cette fonction.  
En regardant la documentation du modem GSM, j'ai trouvé à la section [29.2](https://www.u-blox.com/en/docs/UBX-17003787#%5B%7B%22num%22%3A2890%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C59.527%2C746.177%2Cnull%5D) une commande AT pour obtenir la date et l'heure à partir du GNSS. C'est donc cela que je vais utiliser à la place.
La section [5.3](https://www.u-blox.com/en/docs/UBX-17003787#%5B%7B%22num%22%3A2890%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C59.527%2C746.177%2Cnull%5D) explique également comment lire le RTC du modem GSM. Peut-être que ce RTC peut se mettre à jour avec le 4G même sans accès au GNSS, mais c'est à vérifier.  
La section [5.9](https://www.u-blox.com/en/docs/UBX-17003787) explique comment configurer les mises à jour automatiques du fuseau horaire.
