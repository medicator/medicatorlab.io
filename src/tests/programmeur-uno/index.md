# Programmeur UNO

## But
Pouvoir programmer le ATMEGA328P avec un Arduino UNO.

## Ressources
* [Documentation Arduino sur comment utiliser un ATMEGA328P avec et sans crystal](https://www.arduino.cc/en/Tutorial/ArduinoToBreadboard)
* [MiniCore](https://github.com/MCUdude/MiniCore)
* [Informations sur le débogage de la programmation d'un ATMEGA328P avec un Arduino UNO](https://aherrero.github.io/arduino/iot/2018/10/17/ArduinoToBreadboard-v2.html)

## Schématiques
> ![Image manquante](../../images/burn-atmega-internal-clock.png)  
Télécharger le bootloader avec le crystal interne de 8MHz

> ![Image manquante](../../images/burn-atmega.png)  
Télécharger le bootloader avec un crystal externe de 16MHz

> ![Image manquante](../../images/prog-atmega.png)  
Programmer le microcontrôleur avec le crystal interne

## Démarche
> ![Image manquante](../../images/config-isp-atmega.png)  
Configuration pour le programmeur

> ![Image manquante](../../images/config-arduino-atmega.png)  
Configuration pour burner le Arduino et pour lui uploader des programmes

## Résultat
> ![Image manquante](../../images/delai-blink-on.png)  
Délai du niveau logique haut

> ![Image manquante](../../images/delai-blink-off.png)  
Délai du niveau logique bas

## Code
J'ai téléverser le programme `ArduinoISP` dans le UNO pour programmer le ATMEGA328P.

Pour le ATMEGA328P, j'ai fait mon test avec l'exemple `Blink`.

## Problèmes

### Horloge interne de 8MHz non configurée
Le premier circuit pour télécharger le bootloader ne fonctionnait pas, car l'horloge du Uno était configurée à 16MHz, mais que mon Uno n'était pas configuré pour utiliser son horloge interne.  
Il a donc fallut que je monte le deuxième circuit (j'ai tout simplement remis le microcontrôleur sur une plaquette de Arduino UNO avec un crystal fonctionnel) pour téléverser le nouveau bootloader et tout fonctionnait par la suite. Je peux maintenant utiliser l'ancien ou le nouveau circuit, puisque le UNO utilise désormais son horloge interne.

<details>
<summary>Log du problème initial</summary>

```txt
{{#include log-initial.txt}}
```
</details>

Le message indique que la signature retournée par l'appareil ne correspond pas à celle d'un ATMega328P. En effet, la signature qu'il reçoit est `0x000000` ce qui signifie probablement que le microcontrôleur ne répond pas.  
Le message indique de passer l'option `-F` à la commande pour ignorer le problème, ce que j'ai fait, mais qui n'a pas fonctionné.
<details><summary>Log de la commande avec l'option <code>-F</code>:</summary>

```txt
{{#include log-option-force.txt}}
```
</details>

Ceci montre qu'il a téléversé le nouveau bootloader, mais que lorsqu'il a tenté de vérifier si le bootloader avait été écrit correctement, ce n'était pas le cas.  
J'ai donc fait plus de recherches et j'ai trouvé [ce site](https://aherrero.github.io/arduino/iot/2018/10/17/ArduinoToBreadboard-v2.html) qui explique que cette erreur est due à un problème d'horloge et qu'en ajoutant une horloge pour téléverser le nouveau bootloader va réglé le problème. J'ai donc réalisé le circuit pour téléverser un bootloader avec un crystal et tout a fonctionné cette fois.  
<details><summary>Log de succès:</summary>

```txt
{{#include log-final.txt}}
```
</details>

## Conclusion
Malgré le problème qui est survenu, je suis parvenu à atteindre mon but, mais pas comme je le pensais.

Contrairement à ce que j'avais prévu, il n'est pas nécessaire d'utiliser un UNO pour programmer un ATMEGA328P, seulement pour y téléverser un bootloader. Par la suite, il suffit d'avoir une plaquette de UNO sans microcontrôleur et de connecter les broches `RX`, `TX`, `GND`, `Vcc` et `Reset` de la board au ATMEGA328P. On peut ensuite connecter la plaquette par USB à un ordinateur pour programmer le ATMEGA328P. Ceci fonctionne puisque la plaquette du UNO comporte un circuit pour programmer le UNO.

Une des solutions que j'ai testée pour régler le problème qui survenait pour moi au début était d'utiliser MiniCore, qui est un bootloader pour Arduino qui donne certaines fonctionnalités supplémentaires. Bien que ce ne soit pas lui qui ait réglé mon problème, ses fonctionnalités sont si intéressantes, que je vais continuer à l'utiliser. Il me permet en effet de continuer d'utiliser toutes les librairies faites pour le UNO, d'utiliser les broches du crystal du UNO que je n'utilise pas comme GPIO et me donne accès à une méthode `printf` pour le port sériel. En plus de pouvoir être utilisé dans le Arduino IDE comme je l'ai fait, il peut aussi être utilisé dans PlatformIO IDE.
