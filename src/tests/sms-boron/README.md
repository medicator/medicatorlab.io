# Envoi de SMS à partir du Boron

## But
Envoyer un SMS et un SMS multipart.

## Hypothèse
* Il faudra que je mette ma carte SIM dans le boron
* Que je configure le Boron pour qu'il utilise ma carte SIM
* Que je configure l'APN de ma carte SIM
* Que je configure l'envoi de SMS
* Que j'envoi un SMS

Ressources:
* [Cellular API](https://docs.particle.io/reference/device-os/firmware/boron/#cellular)
* [SARA ublox R4 series SMS AT commands](https://www.u-blox.com/en/docs/UBX-17003787#%5B%7B%22num%22%3A1265%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C59.527%2C742.677%2Cnull%5D)

## Démarche

## Résultats

## Analyse

## Conclusion
