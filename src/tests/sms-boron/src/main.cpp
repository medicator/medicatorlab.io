/**
 * @brief Test l'envoi de SMS single part et multi part
 *
 */

#include <Particle.h>
#include <at_parser_impl.h>
//#include <at_parser.h>
#include <at_command.h>
#include <stdio.h>

using namespace particle::detail;

// Ne pas attendre une connexion au réseau avant l'exécution de `setup()`.
SYSTEM_THREAD(ENABLED);

// Ne pas attendre une connexion au cloud.
SYSTEM_MODE(SEMI_AUTOMATIC);

#define NUM_TELEPHONE "18199685918"

char message[100];

void setup() {
  Serial.begin(9600);
  waitFor(Serial.isConnected, 30000);

  Serial.println("En cours d'initialisation");

  // Utiliser la carte SIM externe.
  Serial.println(Cellular.setActiveSim(EXTERNAL_SIM));
  Cellular.setCredentials("sp.mb.com", "", "");
  Serial.println("Carte SIM externe configurée");

  // Se connecter au réseau.
  Cellular.connect();
  while (!Cellular.ready()) {
    delay(1000);
    Serial.println("Connexion au réseau ...");
  }
  Serial.println("Connexion au réseau établie");

  // Configurer le mode texte de SMS.
  Serial.println(Cellular.command("AT+CMGF=1\r"));

  Serial.println("Initialisation terminée");
}


int cb(int type, const char* buf, int len, void*) {
  Serial.printlnf("Type: %i", type);
  Serial.printlnf("Buf: %s", buf);
  Serial.printlnf("len: %i", len);

  return 0;
}

void loop() {
  delay(2000);
  // Lire une ligne entrée par l'utilisateur.
  char c;
  int i = 0;
  do {
    // Attendre le prochain caractère.
    while (Serial.available() == 0) {
    }

    c = Serial.read();
    Serial.print(c);
    message[i] = c;
    i++;
  } while (c != '\n');
  message[i - 2] = 0;

  Serial.print("Envoi du message: ");
  Serial.println(Cellular.command(message));
}
