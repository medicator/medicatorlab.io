# Programmer le ESP32

## But

Pouvoir téléverser un programme sur le ESP32 et qu'il s'exécute.

## Hypothèse

https://docs.platformio.org/en/latest/tutorials/espressif32/arduino_debugging_unit_testing.html

## Démarche

## Résultats

## Analyse

## Conclusion

Pour s'assure que c'est bel est bien le programme téléversé qui s'exécute, il faudra faire attention lors du design du schématique de ne pas utiliser de broches rattachées à des modes de démarrage spéciaux. Ces modes et ces broches peuvent être trouvés [ici](https://github.com/espressif/esptool/wiki/ESP32-Boot-Mode-Selection).
