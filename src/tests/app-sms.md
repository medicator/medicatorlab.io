# Envoyer/Recevoir des SMS à partir d'une application mobile

## But
Écrire une application capable d'envoyer et recevoir des messages textes. Il faudrait que l'application de SMS par défaut n'affiche pas ces messages. Il faudrait également que les données de ces messages soient encryptés et que les numéros de téléphone soient filtrés.

## Hypothèse
Selon Bilal, le taux de deliverance des SMS est extrêmement bas.

https://google-developer-training.github.io/android-developer-phone-sms-course/Lesson%202/2_c_sms_messages.html

https://developer.android.com/reference/android/provider/Telephony.Sms.Intents.html#getMessagesFromIntent(android.content.Intent)

https://developer.android.com/reference/android/telephony/SmsManager.html#sendDataMessage(java.lang.String,%20java.lang.String,%20short,%20byte%5B%5D,%20android.app.PendingIntent,%20android.app.PendingIntent)

## Code
```java
// TODO
```

## Résultats

## Analyse

## Conclusion
Est-ce que je serai capable de recevoir un SMS sans que l'application de SMS par défaut ne l'intercepte.
