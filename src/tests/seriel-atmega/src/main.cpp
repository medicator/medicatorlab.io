/**
 * @file main.cpp
 * @brief Écoute les communications entrantes et ajouter.
 * @target ATmega328P
 */
#include <Arduino.h>

//! Vitesse de communication du port sériel.
long int vitesseComm = 9600;

void setup() { Serial.begin(vitesseComm); }

void loop() {
  Serial.printf("Vitesse de communication %li\n", vitesseComm);

  if (Serial.available()) {
    long int nouvelleVitesse = Serial.readStringUntil('\n').toInt();
    if (nouvelleVitesse > 0) {
      vitesseComm = nouvelleVitesse;
      Serial.begin(vitesseComm);
    }
  }
}
