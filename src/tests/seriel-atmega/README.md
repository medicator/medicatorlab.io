# Port sériel du ATMEGA328P

## But

S'assurer que le port sériel du ATMEGA328P puisse être utilisé pour y afficher des informations de débogage et pour recevoir des commandes. Cela va aider dans le développement, car, en attendant d'implémenter la communication sécurisée entre le module de contrôle et de distribution, je pourrais envoyer des commandes au module de distribution pour qu'il exécute certaines actions.

J'ai déjà utilisé PlatformIO IDE dans le passé, mais ce sera la première fois que je programmerai ce microcontrôleur et que j'utiliserai son moniteur sériel intégré.

## Hypothèse

J'espère que ceci va fonctionner, mais il y a des chances que ce ne soit pas le cas, car l'horloge interne n'est pas assez précise. Je suis parvenu à téléverser et à faire fonctionner l'exemple `Blink` dans le Arduino IDE en utilisant le port sériel pour programmer le microcontrôleur, donc si le port sériel fonctionne pour le programmer, son port sériel devrait fonctionner pour communiquer des informations de débogage.

Je pense qu'il n'y aura aucun problème avec les basses fréquences, inférieures ou égales à celle utilisée pour la programmation par le Arduino IDE (`38 400Hz`), mais que, plus la vitesse de communication sera haute, plus des erreurs de cadrage pourront être observées sur l'oscilloscope. Je tenterai donc d'observer cette limite, si elle existe.

## Démarche

### Schématique (même que le test précédent)

### Code
```folder-viewer
ignore = ["*"]
include = ["src/main.cpp", "platformio.ini"]
```

## Résultats et analyse

Liste des valeurs valides [9 600, 19 200, 38 400, 57 600, 115 200, 250 000, 500 000 et 1 000 000](https://github.com/MCUdude/MiniCore/blob/master/PlatformIO.md#board_uploadspeed).

### 9600, 19 200, 38 400, 57 600 et 115 200

Tout fonctionne, il n'y a aucun problème.

> ![Image Manquante](./images/9600.png)  
9600 baud

> ![Image Manquante](./images/115200.png)  
115 200 baud

### 250 000

L'outil `stty` ne supportait pas cette valeur.

```sh
$ stty -F /dev/ttyACM0 250000
stty: invalid argument ‘250000’
Try 'stty --help' for more information.
```

### 500 000

Tout fonctionne, mais on peut voir que la vitesse est si rapide que le contrôleur commence à morceler les messages pour les envoyer plus vites. Je pense que c'est du au printf qui affiche le texte, puis les nombres.

> ![Image Manquante](./images/500000.png)  
500 000 baud

### 1 000 000
> ![Image Manquante](./images/1000000.png)  
1 000 000 baud

## Problème survenus

### Vitesse de programmation

Programmation. Solution: vitesse de programmation = 38 400, comme dans Arduino.

```sh
{{#include ./log-erreur.txt}}
```

Commande exécutée par Arduino:

```txt
{{#include ../programmeur-uno/log-initial.txt:0}}
```

Ligne ajoutée dans le fichier platformio.ini:

```txt
{{#include ./platformio.ini:34}}
```

### Taille d'un int = 16 bits

lire un `long int` (32 bits dans Arduino), puis le placer dans un `int` (16 bits dans Arduino). 38400 devient 0.

### Changer la vitesse du serial monitor réinitialise le programme

C'est le cas pour PlatformIO et pour Arduino IDE.

Pour régler ce problème, j'ai exécuté cette commande pour changer la vitesse du port sériel dans Linux sans redémarrer le atmega328p: `stty -F /dev/ttyACM0 1000000`.

## Conclusion

Finalement, malgré l'horloge interne, j'ai pu me rendre jusqu'à X baud sans expérimenter de framing errors. Dépassé cette vitesse ...

À mon grand étonnement, PlatformIO IDE a très bien fonctionné et ne m'a pas causé de problèmes autres que pour téléverser mon programme. Une fois que ça a fonctionné, je n'ai pas expérimenté les plantages qui arrivaient pourtant assez fréquemment la dernière fois que je l'ai utilisé. Je suis définitivement satisfait de l'avoir choisi.

C'était vraiment chiant de devoir fermer le serial monitor, changer le baud rate, puis le réouvrir.

Il faudrait qu'on ait un moyen de trigger sur les framing error.
