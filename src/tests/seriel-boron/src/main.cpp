/**
 * @file main.cpp
 * @brief Écoute les communications entrantes et ajouter.
 * @target Particle Boron
 */
#include <Particle.h>

//! Vitesse de communication du port sériel.
long int vitesseComm = 9600;

void setup() { Serial.begin(vitesseComm); }

void loop() {
  Serial.printlnf("Vitesse de comunication %li", vitesseComm);

  if (Serial.available()) {
    long int nouvelleVitesse = Serial.readStringUntil('\n').toInt();
    if (nouvelleVitesse > 0) {
      vitesseComm = nouvelleVitesse;
      Serial.begin(vitesseComm);
    }
  }
}
