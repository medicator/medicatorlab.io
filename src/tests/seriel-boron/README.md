# Port sériel du Boron

## But
* Pouvoir téléverser et exécuter un programme sur le boron.
* Pouvoir écrire et lire sur le port sériel

## Hypothèse
* [tutoriel de particle](https://docs.particle.io/quickstart/boron/)
* [tutoriel de PlatformIO](https://docs.platformio.org/en/latest/boards/nordicnrf52/particle_boron.html)
    * [blink](https://github.com/platformio/platform-nordicnrf52/tree/master/examples/zephyr-blink)

* [Référence de la classe `Serial`](https://docs.particle.io/reference/device-os/firmware/boron/#serial)

## Démarche

## Résultats
On m'a demandé de configurer mon compte Particle avec une carte de crédit, et j'ai donc décidé que je n'en avait pas besoin pour l'instant. J'ai donc changer de tutoriel pour utiliser celui de PlatformIO. Cela signifie que je ne pourrais pas utiliser leur [WebIDE](https://docs.particle.io/quickstart/boron/#2-open-the-web-ide), mais je comptait utiliser PlatformIO de toutes les façons.

## Analyse
le port utilisé est /dev/ttyACM0 comme pour le ATMEGA328P, ce qui est ennuyant, car, précédemment, je pouvais facilement différencier mon ATMEGA de mon ESP32, car le ESP32 utilisait le port /dev/ttyUSB0. Je pouvais donc configuré PlatformIO pour qu'il téléverse les programmes de ESP32 sur /dev/ttyUSB0 et ceux pour Uno sur /dev/ttyACM0 sans qu'il me demande de choisir un port à chaque fois. Maintenant le port à utiliser va dépendre de l'ordre dans lequel je vais connecter le Boron et le ATMEGA.


#### Problème avec PlatformIO:

```txt
> Executing task in folder seriel-boron: platformio run <

Processing particle_boron (platform: nordicnrf52; board: particle_boron; framework: zephyr)
----------------------------------------------------------------------------------
Verbose mode can be enabled via `-v, --verbose` option
CONFIGURATION: https://docs.platformio.org/page/boards/nordicnrf52/particle_boron.html
PLATFORM: Nordic nRF52 4.0.0 > Particle Boron
HARDWARE: NRF52840 64MHz, 243KB RAM, 796KB Flash
DEBUG: Current (jlink) External (jlink)
PACKAGES: toolchain-gccarmnoneeabi 1.80201.181220 (8.2.1), framework-zephyr 1.20100.200106 (2.1.0), framework-zephyr-hal-nordic 0.20100.191028 (2.1.0), framework-zephyr-civetweb 0.20100.190807 (2.1.0), framework-zephyr-fatfs 0.20100.190522 (2.1.0), framework-zephyr-libmetal 0.20100.190530 (2.1.0), framework-zephyr-lvgl 0.20100.190812 (2.1.0), framework-zephyr-mbedtls 0.20100.191006 (2.1.0), framework-zephyr-mcumgr 0.20100.190528 (2.1.0), framework-zephyr-nffs 0.20100.190523 (2.1.0), framework-zephyr-open-amp 0.20100.190612 (2.1.0), framework-zephyr-openthread 0.20100.191024 (2.1.0), framework-zephyr-segger 0.20100.190421 (2.1.0), framework-zephyr-tinycbor 0.20100.191016 (2.1.0), framework-zephyr-littlefs 0.20100.190811 (2.1.0), framework-zephyr-mipi-sys-t 0.20100.191024 (2.1.0), tool-sreccat 1.164.0 (1.64), tool-cmake 3.15.5, tool-dtc 1.4.7, tool-ninja 1.7.1, tool-gperf 3.0.4
Reading CMake configuration...
-- Selected BOARD particle_boron
-- Loading /home/zakcodes/.platformio/packages/framework-zephyr/boards/arm/particle_boron/particle_boron.dts as base
Devicetree configuration written to /home/zakcodes/cegep/5/pps/esp/rapport/src/tests/seriel-boron/.pio/build/particle_boron/zephyr/include/generated/generated_dts_board.conf
Parsing Kconfig tree in /home/zakcodes/.platformio/packages/framework-zephyr/Kconfig
Loaded configuration '/home/zakcodes/cegep/5/pps/esp/rapport/src/tests/seriel-boron/.pio/build/particle_boron/zephyr/.config'
No change to '/home/zakcodes/cegep/5/pps/esp/rapport/src/tests/seriel-boron/.pio/build/particle_boron/zephyr/.config'
-- Cache files will be written to: /home/zakcodes/.cache/zephyr
-- Configuring done

Zephyr version: 2.1.0
CMake Error at /home/zakcodes/.platformio/packages/framework-zephyr/cmake/extensions.cmake:372 (add_library):
  No SOURCES given to target: app
Call Stack (most recent call first):
  /home/zakcodes/.platformio/packages/framework-zephyr/cmake/app/boilerplate.cmake:538 (zephyr_library_named)
  CMakeLists.txt:2 (include)


CMake Generate step failed.  Build files cannot be regenerated correctly.
```

pour le code:
```cpp
// main.ino

void setup() {}
void loop() {}
```

```txt

```

pour le code:
```cpp
// main.ino

#include <Particle.h>

void setup() {}
void loop() {}
```

aucun problème pour le code:
```cpp
// main.c ou main.cpp
void main() {}
```

Ceci montre que j'aurais du utiliser la framework particle plutôt que zephyr, mais je ne peux pas le faire, car ce [n'est pas supporté par PlatformIO](https://github.com/platformio/platformio-core/issues/445).

Solution: Utiliser le [Particle Workbench](https://docs.particle.io/tutorials/developer-tools/workbench/), une autre extension pour VSCode

#### Le mauvais appareil était configuré
J'avais laissé l'appareil par défaut configuré dans le Particle Workbench et c'était le photon et non le boron, donc je pouvais compiler le programme, mais j'avais une erreur lorsque je le téléversais:
```txt
make -f '/home/zakcodes/.particle/toolchains/buildscripts/1.8.0/Makefile' flash-user -s <


:::: PUTTING DEVICE INFO DFU MODE

Done.

:::: FLASHING APPLICATION

   text    data     bss     dec     hex filename
   5020     108    1420    6548    1994 /home/zakcodes/Downloads/waddup/target/1.4.4/photon/waddup.elf
dfu-suffix (dfu-util) 0.9

Copyright 2011-2012 Stefan Schmidt, 2013-2014 Tormod Volden
This program is Free Software and has ABSOLUTELY NO WARRANTY
Please report bugs to http://sourceforge.net/p/dfu-util/tickets/

Suffix successfully added to file
Serial device PARTICLE_SERIAL_DEV : not available
Flashing using dfu:
dfu-util 0.9

Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
Copyright 2010-2016 Tormod Volden and Stefan Schmidt
This program is Free Software and has ABSOLUTELY NO WARRANTY
Please report bugs to http://sourceforge.net/p/dfu-util/tickets/

Opening DFU capable USB device...
ID 2b04:d00d
Run-time device DFU version 011a
Claiming USB DFU Interface...
Setting Alternate Setting #0 ...
Determining device status: state = dfuIDLE, status = 0
dfuIDLE, continuing
DFU mode device DFU version 011a
Device returned transfer size 4096
DfuSe interface name: "Internal Flash   "
Downloading to address = 0x080a0000, size = 5128
_dfu-util: Last page at 0x080a1407 is not writeable
make[2]: *** [program-dfu] Error 74
make[1]: *** [modules/photon/user-part] Error 2
make: *** [flash-user] Error 2
The terminal process terminated with exit code: 2
```
Comme on peut le voir, le message ne mentionne pas que le mauvais appareil est configuré. Heureusement, j'ai fini par voir qu'il était écrit `photon` plutôt que `boron` dans le chemin du fichier à téléverser et j'ai alors compris mon erreur.

#### Le port sériel ne fonctionne pas
L'appareil est coincé dans le mode [`écoute`](https://docs.particle.io/tutorials/device-os/led/boron/#listening-mode) et ne veut pas en sortir. Ceci est du au fait que je ne l'ai pas configuré avec l'application mobile, car je ne voulais pas y mettre ma carte de crédit. Pour régler ce problème, il a fallut que je suive les étapes vues [ici](https://docs.particle.io/support/particle-devices-faq/mesh-setup-over-usb/#marking-setup-done).

#### Le programme ne démarre pas tant qu'une connection au réseau cellulaire n'a pas été établie.
Il faudrait régler ce problème, car le programme peut fonctionner même sans se connecter au réseau cellulaire.

#### Je n'ai plus de terminal facilement accessible avec un seul bouton
Parce que Workbench n'a pas cette fonctionnalité, j'utiliser miniterm.py, qui est le même programme utilisé par PlatformIO.
```sh
miniterm.py /dev/ttyACM0 9600
```

#### Changer la vitesse du port sériel USB à autre chose que 9600 ne fonctionne pas
J'ai utiliser le même programme que j'avais utilisé pour le UNO et ça ne semble pas fonctionné. J'ai tenter de changer la vitesse à 115200 et de configuré mon port sériel à 9600, mais je pouvais encore voir ce qui était envoyé par le boron alors que je ne devrais pas. J'ai donc vérifier la configuration de mon port sériel (de mon ordi) (`stty << /dev/ttyACM0`), ce qui m'a confirmé qu'il était bel et bien à 9600 baud.



## Conclusion
