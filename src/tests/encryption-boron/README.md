# Encryption AES 256 CBC du Boron

## But
* Pouvoir ajouter une librairie à mon projet
* Pouvoir communiquer de manière encryptée avec mon ATMEGA328P

## Hypothèse
Je vais utiliser le même programme que j'utilise pour le ATMEGA328P, mais je vais changer la librairie d'encryption pour une librairie disponible pour Particle.

Je vais utiliser l'outil en ligne de commande `particle-cli` pour trouver la librairie dont j'ai besoin et pour l'ajouter à mon projet avec les commandes `particle library search <nom>` et `particle library add <nom>`.

Contrairement au ESP32, le module de cryptographie du (nRF52840)[https://www.nordicsemi.com/Products/Low-power-short-range-wireless/nRF52840] ne supporte pas l'encryption AES256 CBC. Il ne supporte que AES 128. J'utiliserai donc une librairie qui n'y fait pas appel comme celle que j'utilise pour le ATMEGA328P.

## Démarche

## Résultats

## Analyse

#### Je n'arrive pas à chercher une librairie
J'obtient l'erreur suivante lorsque j'exécute `particle library search AES_GCM`:
```txt
HTTP error 400 from https://api.particle.io/v1/libraries?filter=AES - The access token was not found
```
Ceci me laisse savoir qu'il me faut un compte pour pouvoir lancer une recherche.

Je suis donc aller sur leur [forum](https://community.particle.io/) pour me créer un compte. Ensuite je suis retourné dans la ligne de commande et j'ai lancé la commande `particle login` qui m'a demandé d'entrer mon l'adresse courriel et le mot de passe de mon compte. Après cela, la commande qui échouait précédemment a fonctionné.

#### AES256 CBC n'est pas supporté par la seul librairie que j'ai trouvée
La seule librairie d'encryption AES que j'ai trouvée est `AES_GCM`.
AES GCM est plus récent que AES CBC et a l'avantage de signer les données qui sont encryptées. Ce n'est pas vraiment un avantage dans notre cas puisqu'on utilisait déjà un CRC pour vérifier que toutes les données décryptées correspondaient aux données avant qu'elles soient encryptées.

Heureusement, la librairie que j'utilise pour le ATMEGA328P supporte AES GCM. Je vais donc aller faire une seconde version de mon code pour le ATMEGA328P qui utilise AES GCM pour la tester avec le Boron.

#### Il n'y a pas de librairie pour calculer un CRC
`particle library search crc` et `particle library search hash` ne retournent aucun résultat.

Ma solution est de copier la librairie [CRC32] que j'utilise pour ATMEGA328P et l'adapter à Particle. Pour ce faire, je vais suivre les instructions qui sont dans le `README.md` d'un projet créé avec Particle:

> If your project includes a library that has not been registered in the Particle libraries system, you should create a new folder named `/lib/<libraryname>/src` under `/<project dir>` and add the `.h`, `.cpp` & `library.properties` files for your library there. Read the [Firmware Libraries guide](https://docs.particle.io/guide/tools-and-features/libraries/) for more details on how to develop libraries. Note that all contents of the `/lib` folder and subfolders will also be sent to the Cloud for compilation.

La librairie [CRC32] est déjà bien configurée pour Particle, puisqu'elle contient déjà un fichier [`library.properties` configuré pour la version 1.5.x du Arduino IDE](https://github.com/arduino/Arduino/wiki/Arduino-IDE-1.5:-Library-specification). Ceci est une bonne nouvelle, puisque tous les projets possèdant ce fichier devraient donc être compatibles avec Particle!

### `AES_GCM::decrypt` fait crasher l'ISR `onReceive`
J'ai d'abord assumé que le problème était l'allocation dynamique de mémoire, puisque le code suivant dans l'ISR le faisait aussi planter:
```cpp
free(malloc(1));
```

Ce n'est cependant pas la seule possibilité. Voici la liste des choses à ne pas faire dans un ISR selon Particle:

* Any memory allocation or free: `new`, `delete`, `malloc`, `free`, `strdup`, `etc`.
* Any `Particle` class function like `Particle.publish`, `Particle.subscribe`, etc.
* Most API functions, with the exception of `pinSetFast`, `pinResetFast`, and `analogRead`.
* delay or other functions that block
* `Log.info`, `Log.error`, etc.
* `sprintf`, `Serial.printlnf`, etc. with a `%f` (`float`) value.

Cependant j'ai observé le code source de cette méthode et elle ne fait pas d'allocation dynamique de mémoire. Il est toutefois possible qu'une des fonctions qu'elle appelle en fasse, mais je n'en
ai pas l'impression.

Il est également possible que je dépasse la taille de la pile allouée à l'ISR.

Dans ces deux cas, la solution que j'ai est la même que j'ai utilisé pour le ESP32 qui m'avait causé le même problème: créer un nouveau thread pour décrypter les données.
Comme l'OS utilisé par Particle est FreeRTOS, je pourrais utiliser les mêmes fonctions que pour le ESP32. Mon code pour le nouveau thread devrait ressembler à ceci:
```
#include <FreeRTOS.h>
#include <task.h>

void decrypter(void*) {
    // Decrypter les données ...

    // Supprimer le thread.
    vTaskDelete(NULL);
}

void onReceive() {
    // Recevoir les données ...

    // Démarrer le thread pour décrypter les données.
    xTaskCreate(decrypter, ...);
}
```

### Je ne peux pas appelé directement les fonctions de FreeRTOS
Même si le code ci-haut ne cause pas d'erreur de compilation, il cause des erreurs de liage. En effet, comme le message ci-bas le montre, Particle ne semble pas lié l'application du client à FreeRTOS:
```txt
../../../build/target/user/platform-13-m/encryption-boron//libuser.a(main.o): In function `decrypter(void*)':
/home/zakcodes/cegep/5/pps/esp/rapport/src/tests/encryption-boron//src/main.cpp:238: undefined reference to `vTaskDelete'
../../../build/target/user/platform-13-m/encryption-boron//libuser.a(main.o): In function `onReceive(int)':
/home/zakcodes/cegep/5/pps/esp/rapport/src/tests/encryption-boron//src/main.cpp:242: undefined reference to `xTaskCreate'
```

Pourtant, en regardant dans le code de deviceOS, je pouvais voir plusieurs appels à ces fonctions, donc je pensais que j'aurais du y avoir accès.

J'ai donc commencé à fouiller dans leur code et leur documentation et j'ai fini par trouver un guide sur comment ajouter un nouveau modèle à deviceOS. Ils y explique qu'il faut y écrire une HAL (hardware abstraction library). J'ai donc observer les fichiers des HALs et j'y ai trouvé le fichier `concurrent_hal.h` qui contient les routines `os_thread_create` et `os_thread_exit` qui sont des abstractions de `xTaskCreate` et `vTaskDelete`. Ces routines prennent dailleurs les mêmes arguments.

J'ai tenté des les utiliser dans mon code et, finalement, tout a fonctionné.

### Je ne sais pas comment obtenir des détails sur les erreurs qui font planter le programme
Lorsque le programme plante, la LED d'état du Boron clignotte en rouge pendant quelques secondes, puis l'appareil redémarre.

Cependant dès qu'il plante, il se déconnecte du port sériel, donc je ne peux recevoir aucun message d'erreur.

Il faudrait donc que je trouve s'il y a un moyen d'obtenir un message sur l'erreur qui vient de survenir, car sinon j'aurais beaucoup de problèmes à déboguer mes programmes.

Il existe un [`Safe Mode`](https://docs.particle.io/tutorials/device-os/device-os/#safe-mode), mais il permet seulement de démarrer l'appareil pour qu'il se connecte au nuage sans exécuter le programme téléversé par l'utilisateur.

S'il n'y a aucune autre solution je pourrais toujours décoder la signification des clignotements en me fiant à [ce guide](https://docs.particle.io/tutorials/device-os/led/boron/#red-flash-sos).

<!-- TODO trouver une meilleure solution -->

### Le code dans `setup` ne s'exécute qu'une fois que l'appareil s'est connecté au réseau cellulaire
Ceci est extrêmement gênant pendant que je programme le système, car je dois attendre 15-30s pour qu'il se connecte avant de voir que mon programme fonctionne ou non.
De plus, mon système pourra continuer à fonctionner même sans connexion, mais en ce moment, si la connection est perdue, mon programme sera arrêté en attendant que la connexion au réseau soit rétablie.

Pour [désactiver ceci](https://docs.particle.io/reference/device-os/firmware/boron/#system-thread) il suffit d'ajouter cette ligne après les inclusions au début du fichier:
```cpp
SYSTEM_THREAD(ENABLED);
```

Je vais même l'ajouter aux programmes que j'ai fait précédemment pour le Boron, car ils n'ont pas besoin d'une connexion réseau avant de commencer à s'exécuter.

### `AES_GCM::decrypt` retourne 160 alors qu'il devrait retourner `true` ou `false`, soit 0 ou 1
Bien que 160 compte comme `true` j'envoi directement la valeur retournée par `AES_GCM::decrypt` en tant que charactère sur le port sériel comme code d'erreur au ATMEGA328P pour lui signifier que le message a pu ou non être décrypté. Cependant, le code que je reçoit au niveau du ATMEGA, n'est pas 0 ou 1, mais 160.

À la fin de `AES_GCM::decrypt` aucune valeur n'est retournée si aucune erreur ne survient. C'est donc pour cela que j'obtenais un résultat aléatoire. Ajouter `return true;` a donc réglé mon problème.

## Conclusion

[CRC32]: https://github.com/bakercp/CRC32/
