/**
 * @brief Test la communication I²C en tant que maître et en tant qu'esclave.
 *
 * Par défaut le contrôleur est en mode esclave et est prêt à accepter des
 * messages. Il ne se met en mode maître que lorsque nécessaire.
 *
 * Lorsqu'il est en mode esclave (à l'adresse 9), il affiche les données qu'il
 * reçoit sur le port sériel et répond aux requêtes de données avec un message
 * qui peut être configuré.
 *
 * Lorsqu'il reçoit 'ecrire <msg>' sur son port sériel, le contrôleur se
 * configure en tant que maître et envoi le message donné à l'esclave à
 * l'adresse ADRESSE_PAIR.
 *
 * Lorsqu'il reçoit 'lire <msg>', le contrôleur se configure en tant que maître
 * et envoi le message donné à l'esclave à l'adresse ADRESSE_PAIR puis lit sa
 * réponse et l'affiche.
 *
 * Lorsqu'il reçoit un message formatté de n'importe quelle autre façon, le
 * message donné est utilisé pour répondre aux prochaines requêtes qui lui sont
 * envoyées lorsqu'il est en mode esclave.
 */

#include <AES_GCM.h>
#include <Particle.h>
#include <Wire.h>
#include <concurrent_hal.h>

//#include <nrf5_sdk/external/mbedtls/include/mbedtls/gcm.h>

// Ne pas attendre une connexion au réseau avant l'exécution de `void setup()`.
SYSTEM_THREAD(ENABLED);

//! Taille de la clé d'encryption
#define TAILLE_CLE_BITS 256
//! Taille de la clé d'encryption
#define TAILLE_CLE (TAILLE_CLE_BITS / 8)
//! Taille d'un bloc
#define AES_BLOCKLEN 16
//! Taille du vecteur d'initialisation AES
#define TAILLE_IV 12
//! Taille du tag GCM
#define TAILLE_TAG_GCM 16
//! Taille des données encryptées pour la structure TMsg
#define TAILLE_DONNEES (10 * AES_BLOCKLEN)
//! Taille d'un paquet transféré sur le port I²C
#define TAILLE_SEGMENT 32

//! Message transféré sur le port I²C
struct TMsg {
  //! Clé d'encryption AES 256
  uint8_t cle[TAILLE_CLE];
  //! Vecteur d'initialisation
  uint8_t iv[TAILLE_IV];
  //! Tag GCM
  uint8_t tag[TAILLE_TAG_GCM];
  //! Données encryptées
  uint8_t donnees[TAILLE_DONNEES];
};

//! Message à envoyer.
TMsg msg;

//! Message décryté transféré.
static char message[TAILLE_DONNEES];

//! Adresse de ce contrôleur en mode esclave.
static const int ADRESSE = 9;
//! Adresse de l'esclave.
static const int ADRESSE_PAIR = 8;

//! Contexte de décryption.
// mbedtls_gcm_context ctxDecryption;

void onReceive(int octets);
void onRequest();
void preparerMsg();

void setup() {
  Serial.begin(9600);

  Wire.begin(ADRESSE);
  Wire.onReceive(onReceive);
  Wire.onRequest(onRequest);

  Serial.println("Initialisation terminée");
}

void erreurEndTransmission(byte b) {
  const char *message;
  switch (b) {
  case 0:
    message = "Aucune erreur de transmission survenue";
    break;
  case 1:
    message = "Trop de données pour le buffer I²C";
    break;
  case 2:
    message = "NACK reçu lors de la transmission de l'adresse";
    break;
  case 3:
    message = "NACK reçu lors de la transmission des données";
    break;
  default:
    message = "Erreur inconnue survenue";
  }
  Serial.println(message);
}

void loop() {
  // Lire une ligne entrée par l'utilisateur.
  char c;
  int i = 0;
  do {
    // Attendre le prochain caractère.
    while (Serial.available() == 0) {
    }

    c = Serial.read();
    Serial.print(c);
    message[i] = c;
    i++;
  } while (c != '\n');
  message[i - 2] = 0;

  Serial.print("Envoi du message: ");
  Serial.println(message);

  // Préparer le message à envoyer.
  preparerMsg();

  Wire.begin();

  // Envoyer le message.
  for (size_t i = 0; i < sizeof(TMsg); i += TAILLE_SEGMENT) {
    int taille = sizeof(TMsg) - i;
    if (taille > TAILLE_SEGMENT) {
      taille = TAILLE_SEGMENT;
    }

    Wire.beginTransmission(ADRESSE_PAIR);
    Wire.write(&((uint8_t *)&msg)[i], taille);
    erreurEndTransmission(Wire.endTransmission());
  }

  // Lire la réponse.
  Wire.requestFrom(ADRESSE_PAIR, 1);
  Serial.print("Code d'erreur reçu: ");
  Serial.println(Wire.read());

  Wire.begin(ADRESSE);
}

volatile bool transfertPrecedentReussi = false;
uint8_t messageDecrypte[TAILLE_DONNEES + 1];

os_thread_t thread;
void decrypterMsg(void *) {
  // Décrypter les données.
  AES_GCM ctx(msg.cle, TAILLE_CLE_BITS);
  transfertPrecedentReussi =
      ctx.Decrypt(TAILLE_DONNEES, msg.donnees, messageDecrypte, TAILLE_TAG_GCM,
                  msg.tag, TAILLE_IV, msg.iv, 0, NULL);

  // Afficher le message reçu.
  messageDecrypte[TAILLE_DONNEES] = 0;
  Serial.print("Message reçu: ");
  Serial.println((char *)messageDecrypte);

  os_thread_exit(NULL);
}

/**
 * @brief Reçoit un message du port I²C et l'envoi sur le port sériel.
 */
void onReceive1(
    //! Nombre d'octets reçus
    int octets) {
  // Index dans le tampon de réception.
  static size_t iRx = 0;

  // Placer toutes les données reçues dans le tampon.
  while (Wire.available() > 0 && iRx < sizeof(TMsg)) {
    ((uint8_t *)&msg)[iRx] = Wire.read();
    iRx++;
  }
  // Ignorer le reste des données reçues.
  while (Wire.available() > 0) {
    Wire.read();
  }

  if (iRx >= sizeof(TMsg)) {
    iRx = 0;
    os_thread_create(&thread, "decrypter_msg", OS_THREAD_PRIORITY_DEFAULT + 1,
                     decrypterMsg, NULL, OS_THREAD_STACK_SIZE_DEFAULT);
  }
}

/**
 * @brief Envoi la variable `reponse` sur le port sériel.
 */
void onRequest() {
  Wire.print((char)transfertPrecedentReussi);
  // Wire.print((char)0);
  transfertPrecedentReussi = false;
}

/**
 * @brief Prépare la structure `msg` en calculant son CRC et en encryptant ses
 * données.
 */
void preparerMsg() {
  // Générer une clé aléatoire.
  for (int i = 0; i < TAILLE_CLE; i++) {
    msg.cle[i] = (uint8_t)rand();
  }

  // Générer un IV aléatoire.
  for (int i = 0; i < TAILLE_IV; i++) {
    msg.iv[i] = (uint8_t)rand();
  }

  // Copier le IV dans un second tampon (afin qu'il ne soit pas modifié par
  // AES_GCM::Encrypt).
  uint8_t iv[TAILLE_IV];
  memcpy(iv, msg.iv, TAILLE_IV);

  // Encrypter les données.
  AES_GCM ctx(msg.cle, TAILLE_CLE_BITS);
  ctx.Encrypt(TAILLE_DONNEES, (uint8_t *)message, msg.donnees, TAILLE_TAG_GCM,
              msg.tag, TAILLE_IV, iv, 0, NULL);
}

#include <FreeRTOS.h>
#include <task.h>

void decrypter(void*) {
  vTaskDelete(NULL);
}

void onReceive(int octets) {
  xTaskCreate(decrypter, "decrypter", 1024, NULL, 5, NULL);
}
