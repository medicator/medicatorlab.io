# Communication sécurisée entre les modules

## But
Combiner ensemble [le test de communication I²C](./I2C-ESP32-ATMEGA328P.md) et ceux d'encryption AES256 du [ATMEGA328P](./encryption-ATMEGA328P.md) et [ESP32](./encryption-ESP32.md).

## Hypothèse
Ça devrait fonctionner sans trop de problème. Il sera probablement nécessaire de configurer les modules d'encryption et de décryption des deux microcontrôleurs pour qu'ils utilisent le même type d'encryption.

## Résultats

## Analyse

## Conclusion
