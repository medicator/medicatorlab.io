/**
 * @file main.cpp
 * @brief Met une liste de sorties à 1 puis à 0.
 * @target ATmega328P
 */
#include <Arduino.h>

enum TMode {
  mSortiesDigitales = 0,
  mSortiesPWM = 1,
  mEntreesDigitales = 2,
  mEntreesAnalogiques = 3,

  mPremier = mSortiesDigitales,
  mDernier = mEntreesAnalogiques,
};

static const char *MODES[] = {
    "sorties digitales",
    "sorties pwm",
    "entrees digitales",
    "entrees analogiques",
};

const int SORTIES_DIGITALES[] = {
    2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
};

const int SORTIES_PWM[] = {
    3, 5, 6, 9, 10, 11,
};

const int ENTREES_DIGITALES[] = {
    2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, A0, A1, A2, A3, A4, A5,
};

const int ENTREES_ANALOGIQUES[] = {
    A0, A1, A2, A3, A4, A5,
};

TMode mode = mSortiesDigitales;

void configurerSortiesDigitales() {
  for (int broche : SORTIES_DIGITALES) {
    pinMode(broche, OUTPUT);
    digitalWrite(broche, 0);
  }
}

void configurerSortiesPWM(unsigned int cycleTravail) {
  for (int broche : SORTIES_PWM) {
    pinMode(broche, OUTPUT);
    analogWrite(broche, cycleTravail);
  }
}

void configurerEntreesDigitales() {
  for (int broche : ENTREES_DIGITALES) {
    pinMode(broche, INPUT_PULLUP);
  }
}

void configurerEntreesAnalogiques() {
  for (int broche : ENTREES_ANALOGIQUES) {
    pinMode(broche, INPUT);
  }
}

void setup() {
  Serial.begin(9600);
  configurerSortiesDigitales();
}

void loop() {
  if (Serial.available()) {
    String nouveauMode = Serial.readStringUntil('\n');
    nouveauMode.toLowerCase();

    bool modeChange = false;
    for (int i = 0; i <= mDernier && !modeChange; i++) {
      const char *modeStr = MODES[i];

      if (nouveauMode.startsWith(modeStr)) {
        mode = (TMode)i;
        modeChange = true;

        if (mode == mSortiesDigitales) {
          configurerSortiesDigitales();
          Serial.println("Configurer en mode sorties digitales");
        } else if (mode == mSortiesPWM) {
          int cycleTravail = nouveauMode.substring(strlen(modeStr)).toInt();
          if (cycleTravail < 0 || cycleTravail > 255) {
            Serial.println("Le cycle de travail donné est invalide. Il doit "
                           "être entre 0 et 255 inclusivement.");
            cycleTravail = 255 / 2;
          }
          Serial.printf("Cycle de travail = %i\n", cycleTravail);
          configurerSortiesPWM(cycleTravail);
        } else if (mode == mEntreesDigitales) {
          configurerEntreesDigitales();
          Serial.println("Configurer en mode entrées digitales");
        } else if (mode == mEntreesAnalogiques) {
          configurerEntreesAnalogiques();
          Serial.println("Configurer en mode entrées analogiques");
        }
      }
    }

    if (!modeChange) {
      Serial.println("Le mode donné est invalide.");
    }
  }

  if (mode == mSortiesDigitales) {
    for (int broche : ENTREES_DIGITALES) {
      digitalWrite(broche, 1);
      digitalWrite(broche, 0);
    }
  } else if (mode == mEntreesDigitales) {
    for (int broche : ENTREES_DIGITALES) {
      Serial.printf("%i:%i ", broche, digitalRead(broche));
    }
    Serial.println();
  } else if (mode == mEntreesAnalogiques) {
    for (int broche : ENTREES_ANALOGIQUES) {
      Serial.print(broche);
      Serial.print(':');
      Serial.print(analogRead(broche) * 5.02 / 1023.0, 2);
      Serial.print(' ');
      //Serial.printf("%i:%f ", broche, 0.5); // Ne fonctionne pas à cause du %f!
    }
    Serial.println();
  }
}
